import { Component, OnInit, ViewChild } from '@angular/core';
import { ScheduleComponent, TimeScaleModel, WorkHoursModel, RecurrenceEditor, EventSettingsModel, EventRenderedArgs } from '@syncfusion/ej2-angular-schedule';
import { Reserva } from '../modelos/reserva.data';
import { Laboratorio } from '../modelos/laboratorio.data';
import { Materia } from '../modelos/materia.data';
import { Ciclo } from '../modelos/ciclo.data';
import { Persona } from '../modelos/persona.data';
import { MatSnackBar, MatDialog } from '@angular/material';
import { CicloService } from '../ciclo/ciclo.service';
import { MateriaService } from '../materia/materia.service';
import { PersonaService } from '../persona/persona.service';
import { LaboratorioService } from '../laboratorio/laboratorio.service';
import { SessionManagerService } from '../servicios/session-manager.service';
import { HorarioService } from '../horario/horario.service';
import { ReservaService } from '../reserva/reserva.service';
import { L10n, } from '@syncfusion/ej2-base';
import { Internationalization } from '@syncfusion/ej2-base';
import { applyCategoryColor } from '../reserva/helper';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

  @ViewChild("scheduleObj")
  public scheduleObj: ScheduleComponent;
  laboratorioSeleccionado: any;
  cicloSeleccionado: any;
  public selectedDate: Date=new Date()
  public timeScale: TimeScaleModel = { enable: true, interval: 60, slotCount: 1 };
  public workWeekDays: number[] = [1, 2, 3, 4, 5, 6];
  public scheduleHours: WorkHoursModel = { highlight: false, start: '6:00', end: '22:00' };
  public showWeekend: boolean = false;
  recurElement: HTMLElement
  recurrObject: RecurrenceEditor
  listaTodosHorario: Reserva[] = [] as Reserva[]
  listaHorariosMapeada: any[] = []
  listaHorariosMapeadaSinFiltro: any[] = []
  listaHorariosMapeadaFiltrada: any[] = []

  fin_recurrenceEditor: string = "UNTIL="
  dia_recurrenceEditor: string = "BYDAY="

  listaCiclos: Ciclo[] = [] as Ciclo[]
  listaLaboratorios: Laboratorio[] = [] as Laboratorio[]
  listaMateria: Materia[] = [] as Materia[]
  listaProfesores: Persona[] = [] as Persona[]
  auxListaProfesores: any[] = [] as any[]
  auxListaMaterias: any[] = [] as any[]
  auxListaLaboratorio: any[] = [] as any[]
  auxListaCiclos: any[] = [] as any[]
  reservas: Reserva[] = [] as Reserva[]
  cicloSeleccionado_modelo: Ciclo = {} as Ciclo
  materiaData: { [key: string]: Object }[]
  laboratorioData: { [key: string]: Object }[]
  personaData: { [key: string]: Object }[]
  cicloData: { [key: string]: Object }[]
  cicloTempral: Ciclo = {} as Ciclo
  laboratorioTemporal: Laboratorio = {} as Laboratorio
  recurrenceEditor_Fin: string = ""
  tipo: string = "H"
  esMovil: boolean = false;

  source: any[] = [

  ]
  public eventSettings: EventSettingsModel = {

    dataSource: [],
    fields: {
      id: 'Id',
      subject: { name: 'Subject', validation: { required: true } }
    }

  };

  constructor(private _snackBar: MatSnackBar, private _cicloService: CicloService, private _materiaService: MateriaService,
    private _laboratorioService: LaboratorioService, private _personaService: PersonaService,
    private _sessionManagerService: SessionManagerService, private _horarioService: HorarioService,
    public matDialog: MatDialog, public _reservaService: ReservaService) {

  }
  ngOnInit() {
    this.esMovil= navigator.userAgent.includes('Mobile')
    L10n.load({
      "es": {
        "schedule": {
          "day": "Día",
          "week": "Semana",
          "workWeek": "Semana laborable",
          "month": "Mes",
          "agenda": "Agenda",
          "weekAgenda": "Semana de Agenda",
          "workWeekAgenda": "Semana de Agenda Laboral",
          "monthAgenda": "Month Agenda",
          "today": "Hoy",
          "noEvents": "No events",
          "emptyContainer": "There are no events scheduled on this day.",
          "allDay": "Todo el día",
          "start": "Inicio",
          "end": "Fin",
          "more": "más",
          "close": "Cerrar",
          "cancel": "Cancelar",
          "noTitle": "(Sin título)",
          "delete": "Eliminar",
          "deleteEvent": "Eliminar Reserva",
          "deleteMultipleEvent": "Eliminar Múltiples Reservas",
          "selectedItems": "Elementos seleccionados",
          "deleteSeries": "Eliminar series",
          "edit": "Editar",
          "editSeries": "Editar",
          "editEvent": "Editar Reserva",
          "createEvent": "Crear",
          "subject": "Asunto",
          "addTitle": "Agregar título",
          "moreDetails": "Más Detalles",
          "save": "Guardar",
          "editContent": "¿Quiere editar esta reserva o toda la serie?",
          "deleteRecurrenceContent": "¿Quiere eliminar esta reserva o toda la serie?",
          "deleteContent": "¿Está seguro de que desea eliminar esta reserva?",
          "deleteMultipleContent": "Are you sure you want to delete the selected events?",
          "newEvent": "Ingreso de reserva",
          "title": "Título",
          "location": "Ubicación",
          "description": "Descripción",
          "timezone": "Zona horaria",
          "startTimezone": "Start Timezone",
          "endTimezone": "End Timezone",
          "repeat": "Repetir",
          "saveButton": "Guardar",
          "cancelButton": "Cancelar",
          "deleteButton": "Eliminar",
          "recurrence": "Recurrencia",
          "wrongPattern": "The recurrence pattern is not valid.",
          "seriesChangeAlert": "The changes made to specific instances of this series will be cancelled and those events will match the series again.",
          "createError": "The duration of the event must be shorter than how frequently it occurs. Shorten the duration, or change the recurrence pattern in the recurrence event editor.",
          "recurrenceDateValidation": "Some months have fewer than the selected date. For these months, the occurrence will fall on the last date of the month.",
          "sameDayAlert": "Two occurrences of the same event cannot occur on the same day.",
          "editRecurrence": "Editar recurrencia",
          "repeats": "Repeticiones",
          "alert": "Alerta",
          "startEndError": "The selected end date occurs before the start date.",
          "invalidDateError": "The entered date value is invalid.",
          "ok": "Ok",
          "occurrence": "Occurrence",
          "series": "Series",
          "previous": "Anterior",
          "next": "Siguiente",
          "timelineDay": "Timeline Day",
          "timelineWeek": "Timeline Week",
          "timelineWorkWeek": "Timeline Work Week",
          "timelineMonth": "Timeline Month"
        },
        "recurrenceeditor": {
          "none": "Niguno",
          "daily": "Diario",
          "weekly": "Semanal",
          "monthly": "Mensual",
          "month": "Mes",
          "yearly": "Anual",
          "never": "Nunca",
          "until": "Hasta",
          "count": "Número de veces",
          "first": "Primero",
          "second": "Segundo",
          "third": "Tercero",
          "fourth": "Cuarto",
          "last": "Último",
          "repeat": "Repetir",
          "repeatEvery": "Repetir cada",
          "on": "Repetir el día:",
          "end": "Fin",
          "onDay": "Día",
          "days": "Día(s)",
          "weeks": "Semana(s)",
          "months": "Mes(es)",
          "years": "Año(s)",
          "every": "cada",
          "summaryTimes": "vece(s)",
          "summaryOn": "on",
          "summaryUntil": "hasta",
          "summaryRepeat": "Repeticiones",
          "summaryDay": "día(s)",
          "summaryWeek": "semana(s)",
          "summaryMonth": "mes(es)",
          "summaryYear": "año(s)"

        },
        "calendar": {
          "today": "Hoy"
        },
        'formValidator': {
          "required": "Este campo es requerido",
          // "max": "الرجاء إدخال قيمة أقل من أو تساوي {0}",
          // "min": "الرجاء إدخال قيمة أكبر من أو تساوي {0}",
          // "regex": "يرجى إدخال قيمة صحيحة",
          // "tel": "يرجى إدخال رقم هاتف صالح",
          // "pattern": "الرجاء إدخال قيمة نمط صحيح",
          // "equalTo": "يرجى إدخال نص مطابقة صحيح"
        }
      }
    });

    	this.idPersona=this._sessionManagerService.obteneridPersona()
    this.cargarDatos()
  }

  idPersona='';

  cargarDatos() {
    this.obtenerCiclos();
    this.obtenerLaboratorios();
    this.obtenerMaterias();
    this.obtenerPersonal();
    this.eventSettings.dataSource = this.source

  }

  onChangeCiclo(event: any) {
    this.setearFechaCiclo(event.value)
  }

  setearFechaCiclo(idCiclo) {
    this.cicloTempral = this.listaCiclos.find(ciclo => ciclo.id == idCiclo)
    var fechaTemporalFin = this.cicloTempral.fechaFin
    var fechaTemporalInicio = this.cicloTempral.fechaInicio
    this.fin_recurrenceEditor = "UNTIL="
    var fechaFin = new Date(fechaTemporalFin.toString())
    //this.selectedDate = new Date(fechaTemporalInicio.toString())


    var dia = fechaFin.getDate() < 10 ? '0' + fechaFin.getDate() : fechaFin.getDate()
    var mes = fechaFin.getMonth() < 10 ? '0' + (fechaFin.getMonth() + 1) : fechaFin.getMonth()

    this.fin_recurrenceEditor = this.fin_recurrenceEditor.concat(fechaFin.getFullYear().toString()).concat(mes.toString()).concat(dia.toString())
    var x = document.getElementById("finCiclo")

  }
  obtenerMaterias() {
    this._materiaService.obtener().subscribe(
      materias => {
        this.listaMateria = materias
      
      }
    )

  }
  obtenerLaboratorios() {
    this._laboratorioService.obtener().subscribe(
      laboratorios => {
        this.listaLaboratorios = laboratorios.filter(item => { return item.estado == 'ACTIVO' })

       
        this.obtenerHorario()
      
      }
    )
  }
  obtenerHorario() {
    this._horarioService.obtener().subscribe(
      horarios => {
        this.listaTodosHorario = horarios
        this.listaHorariosMapeada = this.listaTodosHorario.filter(
          horario => {
            return horario.fkPersonaReserva.id == this.idPersona
          }
        ).map(
          horario => {

            return this.mapearHorario(horario)
          }
        )
        eventRendered: (args: EventRenderedArgs) => applyCategoryColor(args, this.scheduleObj.currentView)
        this.listaHorariosMapeadaFiltrada = this.listaHorariosMapeada
        this.scheduleObj.eventSettings.dataSource = this.listaHorariosMapeadaFiltrada
        this.listaHorariosMapeadaSinFiltro = this.listaHorariosMapeadaFiltrada
      },
      error => {
        console.log(error);
      }
    )


  }
  mapearHorario(horario: Reserva) {
    return {

      Id: horario.id,
      StartTime: horario.fechaInicio,
      EndTime: horario.fechaFin,
      idUsuario: horario.fkPersonaCrea.id,
      laboratorio: horario.fkLaboratorio.id,
      materia: horario.fkMateria.id,
      profesor: horario.fkPersonaReserva.id,
      RecurrenceRule: horario.recurrenceRule,
      Subject: horario.subject+"\nLaboratorio: "+horario.fkLaboratorio.nombre,
      CategoryColor: horario.tipo == 'H' ? '#0b2136' : '#009688',
      tipo: horario.tipo,



    }
  }
  obtenerPersonal() {
    this._personaService.obtener().subscribe(
      persona => {
        this.listaProfesores = persona
       
      }
    )
  }
  obtenerCiclos() {
    this._cicloService.obtener().subscribe(
      ciclos => {
        this.listaCiclos = ciclos.sort((a, b) => {

          return (new Date(b.fechaInicio.toString()).getTime() - new Date(a.fechaInicio.toString()).getTime());

        })

      }
    )
  }

  public oneventRendered(args: EventRenderedArgs): void {
    let categoryColor: string = args.data.CategoryColor as string;
    if (!args.element || !categoryColor) {
      return;
    }
    if (this.scheduleObj.currentView === 'Agenda') {
      (args.element.firstChild as HTMLElement).style.borderLeftColor = categoryColor;
    } else {
      args.element.style.backgroundColor = categoryColor;
    }
  }
  private instance: Internationalization = new Internationalization();
  getTimeString(value: Date): string {
    return this.instance.formatDate(value, { skeleton: 'hm' });
  }
  profesor(idPersona){
    var nombreProfesor= this.listaProfesores.find(x=>{return x.id==idPersona})
    return nombreProfesor.nombreCompleto
  }


}
