import { Component, OnInit } from '@angular/core';
import { Usuario } from '../modelos/persona.data';
import { UsuarioService } from '../usuario/usuario.service';
import { SessionManagerService } from '../servicios/session-manager.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { SnackbarVistaComponent } from '../snackbar-vista/snackbar-vista.component';
import { PersonaService } from '../persona/persona.service';

@Component({
  selector: 'app-mi-cuenta',
  templateUrl: './mi-cuenta.component.html',
  styleUrls: ['./mi-cuenta.component.css']
})
export class MiCuentaComponent implements OnInit {

  public formAccount: FormGroup;
  usuario:Usuario={};
  emailRegEx = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
  nameRegEx='([A-ZÁÉÍÓÚÑ][a-záéíóúñ]+ ?){2,5}';
  constructor(private _personaService:PersonaService,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private _usuarioService:UsuarioService,
    private _sessionManager:SessionManagerService) { }

  ngOnInit() {
    this.iniciarFormulario();
    this.cargarUsuario();
  }

  mostrarMensaje(mensaje: string, type: string = "Info") {

    this._snackBar.openFromComponent(SnackbarVistaComponent, {
      data: { message: mensaje, snackType: type },
      duration: 5000
    });

  }
  
  iniciarFormulario() {
    this.formAccount = this.formBuilder.group(
      {
        correo: ['', [Validators.required, Validators.pattern(this.emailRegEx)]],
        nombreCompleto: ['', [Validators.required, Validators.pattern(this.nameRegEx)]]
      }
    );
  }


  cargarUsuario(){
    let id=this._sessionManager.obtenerID();
    this._usuarioService.obtenerPorID(id).subscribe(
      usuario=>{
        this.usuario=usuario;
        this.formAccount.controls["correo"].setValue(usuario.fkPersona.correo);
        this.formAccount.controls["nombreCompleto"].setValue(usuario.fkPersona.nombreCompleto);
      },error=>{
        alert("Error al obtener la información del usuario")
      }
    )
  }

  disableSaveButton=false;
  guardarCambios(){
    if(this.formAccount.valid){
      this.disableSaveButton=true;
      let personaTemp={
        id:this.usuario.fkPersona.id,
        correo:this.formAccount.controls["correo"].value,
        nombreCompleto:this.formAccount.controls["nombreCompleto"].value,
      }
      this._personaService.editar(personaTemp).subscribe(
        personaEditada=>{
          this.disableSaveButton=false;
          this.mostrarMensaje("Sus datos han sido actualizado exitosamente","Success");
        },error=>{
          this.disableSaveButton=false;
          this.mostrarMensaje("No se ha podido modificar su información","Warning");
        }
      );
    }else{
      this.mostrarMensaje("La información ingresada no es valida","Warning");
    }

  }

  disableRecoverButton=false;
  cambiarClave(){
    if(confirm("Se enviará un enlace a su correo electrónico para cambiar su clave")){
      
    this.disableRecoverButton = true;
    let correo = this.formAccount.controls['correo'].value;
    this._usuarioService.solicitarRecuperacion(correo).subscribe(
      resultado => {
        if (resultado.estado) {
          this.mostrarMensaje('Se ha enviado un enlace para cambiar su contraseña al correo electrónico: ' + correo, "Success");
        } else {

          this.mostrarMensaje(resultado.mensaje, "Warning")
        }
        this.disableRecoverButton = false;
      }, error => {
        console.log(error)
        this.disableRecoverButton = false;
        this.mostrarMensaje('No se ha podido enviar un correo de recuperación al correo electrónico: ' + correo, "Error");

      }
    )
    }
  }

}
