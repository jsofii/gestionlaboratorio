import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Carrera } from '../modelos/materia.data';
import {MateriaService} from '../materia/materia.service'
import { MatSnackBar } from '@angular/material';
import { SnackbarVistaComponent } from '../snackbar-vista/snackbar-vista.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SistemaOperativoService } from '../servicios/sistema-operativo.service';
import { SistemaOperativo } from '../modelos/sistemaOperativo.data';

@Component({
  selector: 'app-parametros',
  templateUrl: './parametros.component.html',
  styleUrls: ['./parametros.component.css']
})
export class ParametrosComponent implements OnInit {
  @ViewChild('cerrarModalCarrera') cerrarModalCarrera: ElementRef;
  @ViewChild('cerrarModalSO') cerrarModalSO: ElementRef;

  sistemasOperativos:SistemaOperativo[]=[];
  carreras:Carrera[]=[];
  columnasCarreras = [
    { field: 'nombre', header: 'Carrera' },
    { field: 'estado', header: 'Estado' },
    { field: 'acciones', header: 'Acciones' },
    { field: 'activacion', header: 'Activar/Desactivar' }
  ];
  columnasSO = [
    { field: 'nombre', header: 'Nombre' },
    { field: 'estado', header: 'Estado' },
    { field: 'acciones', header: 'Acciones' },
    { field: 'activacion', header: 'Activar/Desactivar' }
  ];
  public formCrearCarrera: FormGroup;
  tituloModalEditarCarrera: string;
  esNuevaCarrera: boolean = false;

  public formCrearSO: FormGroup;
  tituloModalEditarSO: string;
  esNuevoSO: boolean = false;

  constructor(private _materiasService:MateriaService, private _snackBar:MatSnackBar, private fb: FormBuilder,private _sistemaOperativoService:SistemaOperativoService) { }

  ngOnInit() {
    this.cargarCarreras();
    this.cargarSistemasOperativos();
    this.iniciarFormularios();
  }

  iniciarFormularios(){
    this.formCrearCarrera= this.fb.group(
      {
        nombre:['', [Validators.required]],
        estado: [],
        idCarrera: []
      }
    );
    this.formCrearSO= this.fb.group(
      {
        nombre:['', [Validators.required]],
        estado: [],
        idSO: []
      }
    )
  }

  mostrarMensaje(mensaje: string, type: string = "Info") {

    this._snackBar.openFromComponent(SnackbarVistaComponent, {
      data: { message: mensaje, snackType: type },
      duration: 5000
    });

  }

  cargarCarreras(){
    this._materiasService.listaCarreras().subscribe(
      carreras=>{
        this.carreras=carreras;
      },error=>{
        this.mostrarMensaje("Error al cargar las carreras","Error");
        console.log(error);

      }
    );
  }
  cargarSistemasOperativos(){
    this._sistemaOperativoService.obtener().subscribe(
      sistemasOperativos=>{
        this.sistemasOperativos=sistemasOperativos;
      },error=>{
        this.mostrarMensaje("Error al cargar los sistemas operativos","Error");
        console.log(error);

      }
    );
  }
  limpiarFormCrearCarrera(){
    this.formCrearCarrera.patchValue({
      nombre: "",
      estado: ""

    })
  }
  limpiarFormCrearSO(){
    this.formCrearSO.patchValue({
      nombre: "",
      estado: ""

    })
  }
  abrirModalCrearCarrera(){
    this.limpiarFormCrearCarrera()
    this.tituloModalEditarCarrera = "Nueva Carrera"
    this.esNuevaCarrera = true;
  }
  abrirModalCrearSO(){
    this.limpiarFormCrearSO()
    this.tituloModalEditarSO = "Nuevo Sistema Operativo"
    this.esNuevoSO = true;
  }

  abrirModalEditarCarrera(carrera:Carrera){
    this.tituloModalEditarCarrera ="Modificar carrera"
    this.esNuevaCarrera= false
    this.formCrearCarrera.patchValue({
      nombre: carrera.nombre,
      estado: carrera.estado,
      idCarrera: carrera.id

    })
  }
  abrirModalEditarSO(sistemaOperativo:SistemaOperativo){
    this.tituloModalEditarSO ="Modificar sistema operativo"
    this.esNuevoSO= false
    this.formCrearSO.patchValue({
      nombre: sistemaOperativo.nombre,
      estado: sistemaOperativo.estado,
      idSO: sistemaOperativo.id

    })
  }

  guardarCarrera(){
    if(this.esNuevaCarrera){
      this.crearCarrera();
      
    }else{
      this.editarCarrera();
    }
    this.cerrarModalCarrera.nativeElement.click()
  }
  guardarSO(){
    if(this.esNuevoSO){
      this.crearSO();
      
    }else{
      this.editarSO();
    }
    this.cerrarModalSO.nativeElement.click()
  }

  crearCarrera(){
    var carrera: Carrera={
      nombre: this.formCrearCarrera.controls['nombre'].value,
      estado: "ACTIVO"
    }
    
    this._materiasService.registrarCarrera(carrera).subscribe(
      carreraCreada => {
        this.carreras.push(carreraCreada);
        this.cerrarModalCarrera.nativeElement.click()
        this.mostrarMensaje("Carrera registrada exitosamente!","Success")
        this.limpiarFormCrearCarrera();
      },
      error=>{
        console.log(error);
        this.mostrarMensaje("Error al crear la carrera","Error");
      }
    )
  }
  crearSO(){
    var sistemaOperativo: SistemaOperativo={
      nombre: this.formCrearSO.controls['nombre'].value,
      estado: "ACTIVO"
    }
    
    this._sistemaOperativoService.crear(sistemaOperativo).subscribe(
      soCreado => {
        this.sistemasOperativos.push(soCreado);
        this.cerrarModalSO.nativeElement.click()
        this.mostrarMensaje("Sistema Operativo registrado exitosamente!","Success")
        this.limpiarFormCrearSO();
      },
      error=>{
        console.log(error);
        this.mostrarMensaje("Ha surgido un problema con la creación del sistema operativo","Error");
      }
    )
  }
  
  editarCarrera(){
    var carrera: Carrera={
      id: this.formCrearCarrera.controls['idCarrera'].value,
      nombre: this.formCrearCarrera.controls['nombre'].value
    }
    this._materiasService.editarCarrera(carrera).subscribe(
      carreraEditada=>{
        let index = this.carreras.findIndex(carrera=>carrera.id==carreraEditada.id)
        this.carreras[index]=carreraEditada;
        this.mostrarMensaje("Carrera editada exitosamente!","Success")
      },error=>{
        this.mostrarMensaje("Error al modificar la carrera!","Error")
      }
    )

  }
  editarSO(){
    var sistemaOperativo: SistemaOperativo={
      id: this.formCrearSO.controls['idSO'].value,
      nombre: this.formCrearSO.controls['nombre'].value
    }
    this._sistemaOperativoService.editar(sistemaOperativo).subscribe(
      soEditado=>{
        let index = this.sistemasOperativos.findIndex(so=>so.id==soEditado.id)
        this.sistemasOperativos[index]=soEditado;
        this.mostrarMensaje("Sistema Operativo editado exitosamente!","Success")
      },
      error=>{
        this.mostrarMensaje("Error al modificar el sistema operativo!","Error")
      }
    )

  }
  
  desactivarCarrera(carrera: Carrera){
    if(carrera.estado=="INACTIVO"){
      carrera.estado="ACTIVO"
    }else{
      carrera.estado="INACTIVO"
    }

    let carreraTemp:Carrera={
      id:carrera.id,
      estado:carrera.estado
    }
    
    this._materiasService.editarCarrera(carreraTemp).subscribe(
      carreraActualizada=>{
        this.mostrarMensaje("Cambios aplicados exitosamente","Success");
        let index=this.carreras.findIndex(carrera=>carrera.id==carreraActualizada.id);
        this.carreras[index]=carreraActualizada;
      },error=>{
        this.mostrarMensaje("Error al editar los datos de la carrera","Error");
      }
    )
  }
  desactivarSO(sistemaOperativo: SistemaOperativo){
    if(sistemaOperativo.estado=="INACTIVO"){
      sistemaOperativo.estado="ACTIVO"
    }else{
      sistemaOperativo.estado="INACTIVO"
    }

    let soTemp:SistemaOperativo={
      id:sistemaOperativo.id,
      estado:sistemaOperativo.estado
    }
    
    this._sistemaOperativoService.editar(soTemp).subscribe(
      soEditado=>{
        let index = this.sistemasOperativos.findIndex(so=>so.id==soEditado.id)
        this.sistemasOperativos[index]=soEditado;
        this.mostrarMensaje("Sistema Operativo editado exitosamente!","Success")
      },
      error=>{
        this.mostrarMensaje("Error al modificar el sistema operativo!","Error")
      }
    )
  }


}
