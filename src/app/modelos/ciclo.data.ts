import { Reserva } from './reserva.data';
import { SoftwarePorLaboratorio } from './laboratorio.data';

export interface Ciclo{
    id?:number
    createdAt?: number;
    updatedAt?: number;
    nombre?:string;
    fechaInicio?:Date | String;
    fechaFin?:Date | String;
    estado?:'ACTIVO'|'INACTIVO';
    arregloReservas?:Reserva[] | number[] | any[];
    arregloSoftwareLaboratorioCiclo?: SoftwarePorLaboratorio[] | number[] | any[]
}