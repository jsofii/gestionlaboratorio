import { Laboratorio } from './laboratorio.data';
import { Persona } from './persona.data';
import { Materia } from './materia.data';
import { Ciclo } from './ciclo.data';

export interface Reserva{
    id?:number
    createdAt?: number;
    updatedAt?: number;
    fechaInicio?:Date | String;
    fechaFin?:Date | String;
    subject?:string;
    recurrenceRule?:string;
    tipo?:string;
    fkLaboratorio?:Laboratorio|number|any;
    fkPersonaReserva?:Persona|number|any;
    fkPersonaCrea?:Persona|number|any;
    fkMateria?:Materia|number|any;
    fkCiclo?:Ciclo|number|any;
    CategoryColor?: "#0b2136"


    
}
export interface LogReserva{
    id?:number,
    createdAt?: number;
    updatedAt?: number;
    accion?: string, 
    anterior?: string | Reserva;
    actual?:string | Reserva;
    idReserva?:number | any;
    idUsuario?:number | any
    
   
}

