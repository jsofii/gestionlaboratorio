import { Reserva } from './reserva.data';

export interface Persona{
    id?:number
    createdAt?: number;
    updatedAt?: number;
    nombreCompleto?:string;
    fkTipoPersona?:number|TipoPersona|any;
    correo?:string;
    estado?:'ACTIVO'|'INACTIVO';
    arregloUsuarios?:Usuario[]|number[]|any[];
    arregloReservasCreadas?:Reserva[]|number[]|any[];
    arregloReservasAsignadas?:Reserva[]|number[]|any[];

}
export interface TipoPersona{
    id?:number
    createdAt?: number;
    updatedAt?: number;
    nombre?: string
    arregloPersona?:Persona[]|number[]|any[];
}

export interface Usuario{
    id?:number
    createdAt?: number;
    updatedAt?: number;
    estado?:'ACTIVO'|'INACTIVO';
    fkPersona?:Persona|number|any;
    password?:string;

}