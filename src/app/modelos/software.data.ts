import { SoftwarePorLaboratorio } from './laboratorio.data';

export interface Software{
    id?:number
    createdAt?: number;
    updatedAt?: number;
    nombre?:string;
    version?: string;   
    estado?:'ACTIVO'|'INACTIVO';
    arregloSoftwareLaboratorioCiclo?:SoftwarePorLaboratorio[] | number[] | any[];
}