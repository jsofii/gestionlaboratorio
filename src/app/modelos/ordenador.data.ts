import { Laboratorio } from './laboratorio.data';
import { Procesador } from './procesador.data';
import { SistemaOperativo } from './sistemaOperativo.data';


export interface Ordenador{

    createdAt?: number,
    updatedAt?: number,
    id?: number,
    ram: number | String,
    fkLaboratorio: Laboratorio | number ,
    fkProcesador: number | Procesador,
    arregloOrdenadorSistemaOperativo?: any[] | SistemaOperativo[]
    
}