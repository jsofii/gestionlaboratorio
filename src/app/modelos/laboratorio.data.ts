
import {Software} from './software.data'
import { Ciclo } from './ciclo.data';
import { Reserva } from './reserva.data';
import { Ordenador } from './ordenador.data';
import { SistemaOperativo } from './sistemaOperativo.data';

export interface Laboratorio{
    id?:number
    createdAt?: number;
    updatedAt?: number;
    nombre?:string;
    numero?:string;
    estado?:'ACTIVO'|'INACTIVO';
    capacidad?: number | any;
    arregloSoftwareLaboratorioCiclo?:SoftwarePorLaboratorio[] | number[] | any[];
    arregloReservas?:Reserva[] | number[] | any[];
    arregloOrdenador?: Ordenador[] | number[] | any[]
    
}

export interface SoftwarePorLaboratorio{
    id?:number
    createdAt?: number;
    updatedAt?: number;
   
    fkLaboratorio?:Laboratorio|number|any,
    fkSoftware?:Software|number|any
}


export interface LaboratorioFiltro{
    nombreLaboratorio: string,
    idLaboratorio: number,
    capacidad: number,
    cumpleCapacidad: boolean,
    ram: number,
    cumpleRam: boolean,
    sistemasOperativos: string[],
    procesador: string,
    cumpleProcesador: boolean,
    softwareIncluidos?: string[],
    software?: string[],
    softwareLaboratorio?: any[],
    sistemasOperativosIncluidos: number,
    sistemasOperativosLaboratorio: string[]
}