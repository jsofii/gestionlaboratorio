import { Ordenador } from './ordenador.data';

export interface SistemaOperativo{
    
    createdAt?: number,
    updatedAt?: number,
    id?: number,
    estado?:string,
    nombre?: String | any
}
export interface ordenadorSistemaOperativo{
    fkOrdenador: number | any | Ordenador,
    fkSistemaOperativo: SistemaOperativo
    
}