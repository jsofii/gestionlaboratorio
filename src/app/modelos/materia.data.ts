export interface Materia{
    id?:number
    createdAt?: number;
    updatedAt?: number;
    codigo?:string;
    nombre?:string;
    fkCarrera?:Carrera|number|any;
    estado?:'ACTIVO'|'INACTIVO';
}


export interface Carrera{
    id?:number
    createdAt?: number;
    updatedAt?: number;
    nombre?:string;
    estado?:'ACTIVO'|'INACTIVO';
    arregloMaterias?:Materia[] | number[] | any[];
    
}