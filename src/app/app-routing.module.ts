import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { LaboratorioComponent } from './laboratorio/laboratorio.component';
import { HorarioComponent } from './horario/horario.component';
import { SoftwareComponent } from './software/software.component';
import { GestionComponent } from './gestion/gestion.component';
import { PersonaComponent } from './persona/persona.component';
import { MateriaComponent } from './materia/materia.component';
import { ReservaComponent } from './reserva/reserva.component';
import { ConsultaHorarioComponent } from './consulta-horario/consulta-horario.component';
import { ConsultaReservaComponent } from './consulta-reserva/consulta-reserva.component';
import { ReporteHorarioComponent } from './reporte-horario/reporte-horario.component';
import { LoginComponent } from './login/login.component';
import { RecuperacionPasswordComponent } from './recuperacion-password/recuperacion-password.component';
import { MiCuentaComponent } from './mi-cuenta/mi-cuenta.component';
import { CicloComponent } from './ciclo/ciclo.component';
import { ReporteLogsComponent } from './reporte-logs/reporte-logs.component';
import { CalendarioActividadesComponent } from './calendario-actividades/calendario-actividades.component';
import { ParametrosComponent } from './parametros/parametros.component';
import { PrincipalComponent } from './principal/principal.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'recuperacion-password', component: RecuperacionPasswordComponent },
  {
    path: 'app', component: HomeComponent, children: [{
      
      path: 'gestion', component: GestionComponent,
      children: [
        { path: 'personal', component: PersonaComponent },
        { path: 'usuarios', component: UsuarioComponent },
        { path: 'laboratorios', component: LaboratorioComponent },
        { path: 'materias', component: MateriaComponent },
        { path: 'software', component: SoftwareComponent },
        { path: 'horarios', component: HorarioComponent },
        { path: 'reservas', component: ReservaComponent },
        { path: 'ciclos', component: CicloComponent },
        { path: 'parametros', component: ParametrosComponent }
      ]
    },
    { path: 'principal', component: PrincipalComponent },
    { path: 'mi-cuenta', component: MiCuentaComponent },
    {
      path: 'consultas', component: GestionComponent,
      children: [

        { path: 'reservar-laboratorio', component: ReservaComponent },
        { path: 'calendario-actividades', component: CalendarioActividadesComponent },
        { path: 'horarios', component: ConsultaHorarioComponent },
        { path: 'reservas', component: ConsultaReservaComponent },
        { path: 'reporte', component: ReporteHorarioComponent },
        { path: 'reporteLogs', component: ReporteLogsComponent }
      ]
    }
    ],
  },
  
  { path: 'calendario-actividades', component: CalendarioActividadesComponent },
  { path: 'login', component: LoginComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
export const routingComponents = [
  LaboratorioComponent, SoftwareComponent, UsuarioComponent, HorarioComponent
]
