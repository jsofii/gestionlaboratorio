import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmacionEliminarEventoComponent } from './confirmacion-eliminar-evento.component';

describe('ConfirmacionEliminarEventoComponent', () => {
  let component: ConfirmacionEliminarEventoComponent;
  let fixture: ComponentFixture<ConfirmacionEliminarEventoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmacionEliminarEventoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmacionEliminarEventoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
