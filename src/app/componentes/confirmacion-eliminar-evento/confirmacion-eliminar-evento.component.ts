import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-confirmacion-eliminar-evento',
  templateUrl: './confirmacion-eliminar-evento.component.html',
  styleUrls: ['./confirmacion-eliminar-evento.component.css']
})
export class ConfirmacionEliminarEventoComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<ConfirmacionEliminarEventoComponent>,) {
    dialogRef.disableClose = true;
   }

  ngOnInit() {
  }
  eliminar(){
    this.dialogRef.close(true)
  }
  cancelar(){
    this.dialogRef.close(false)
  }
}
