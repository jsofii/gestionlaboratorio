import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltroLaboratorioComponent } from './filtro-laboratorio.component';

describe('FiltroLaboratorioComponent', () => {
  let component: FiltroLaboratorioComponent;
  let fixture: ComponentFixture<FiltroLaboratorioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltroLaboratorioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltroLaboratorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
