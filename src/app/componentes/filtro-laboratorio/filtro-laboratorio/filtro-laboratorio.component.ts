import { Component, OnInit, Inject } from '@angular/core';
import { LaboratorioService } from 'src/app/laboratorio/laboratorio.service';
import { MAT_DIALOG_DATA, MatTableDataSource, MatDialogRef } from '@angular/material';
import { Form, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Laboratorio, LaboratorioFiltro } from 'src/app/modelos/laboratorio.data';
import { Ordenador } from 'src/app/modelos/ordenador.data';
import { DATA_COLLECTION_ENABLED } from '@angular/fire/performance';
import { SoftwareService } from 'src/app/software/software.service';
import { Software } from 'src/app/modelos/software.data';
@Component({
  selector: 'app-filtro-laboratorio',
  templateUrl: './filtro-laboratorio.component.html',
  styleUrls: ['./filtro-laboratorio.component.css']
})
export class FiltroLaboratorioComponent implements OnInit {

  displayedColumns: string[] = ['laboratorio', 'capacidad', 'ram', 'procesador','sistemaOperativo', 'software', 'opciones'];
  public formFiltro: FormGroup
  dataSource: MatTableDataSource<any>;
  listaOrdenadores: Ordenador[] = [] as Ordenador[]
  listaLaboratorios: Laboratorio[] = [] as Laboratorio[]
  listaSofware: Software[] = [] as Software[]
  auxSistemasOperativos: any[] = [] as any[]
  auxProcesador: any[] = [] as any[]
  auxSoftware: any[] = [] as any[]
  listaLaboratorioFiltrada: LaboratorioFiltro[] = [] as LaboratorioFiltro[]
  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<FiltroLaboratorioComponent>,
    private fb: FormBuilder, private _laboratorioService: LaboratorioService, private _softwareService: SoftwareService) {
    this.listaLaboratorios = data.listaLaboratorios
    this.traerOrdenadores()
    this.dataSource = new MatTableDataSource<any>();
  }

  ngOnInit() {
    this.mapearSistemasOperativos()
    this.mapearProcesadores()
    this.mapearSoftware()
    this.inicializarFormFiltro()
  }
  mapearSoftware() {
    this._softwareService.obtener().subscribe(
      result => {
        
        this.listaSofware= result

        this.auxSoftware = result.map(
          item => {
            return {
              label: item.nombre,
              value: item.id
            }
          }
        )
      }
    )
  }
  mapearSistemasOperativos() {
    this._laboratorioService.obtenerSistemasOperativos().subscribe(
      result => {
        this.auxSistemasOperativos = result.map(
          item => {
            return {
              label: item.nombre,
              value: item.id
            }
          }
        )
      }
    )
  }
  mapearProcesadores() {
    this._laboratorioService.obtenerProcesadores().subscribe(
      listaProcesadores => {
        this.auxProcesador = listaProcesadores.map(
          item => {
            return {
              label: item.nombre,
              value: item.id
            }
          }
        )
      }
    )
  }
  traerOrdenadores() {
    this._laboratorioService.obtenerOrdenadores().subscribe(
      listaOrdenadores => {
        this.listaOrdenadores = listaOrdenadores
        this._laboratorioService.obtenerOrdenadorSistemaOperativo().subscribe(
          listaSisOperativos => {
            var listaSOs = listaSisOperativos
            this.listaOrdenadores = listaOrdenadores.map(
              ordenador => {
                var listaSOs = listaSisOperativos
                ordenador.arregloOrdenadorSistemaOperativo = listaSOs.filter(so => {
                  return so.fkOrdenador.id == ordenador.id
                })
                return ordenador
              })
            this.listaLaboratorios.map(
              lab => {
                lab.arregloOrdenador = this.listaOrdenadores.filter(item => {
                  var ordTemp: any = item
                  return ordTemp.fkLaboratorio.id == lab.id
                })
                return lab
              }
            )
            this.listaLaboratorios
          }
        )

      }
    )
  }
  ejecutarFiltro() {
    this.formFiltro.value.sistemaOperativo.length
    this.formFiltro
    this.auxProcesador
    this.listaLaboratorioFiltrada = this.listaLaboratorios.map(
      lab => {
        var capacidad = Number(this.formFiltro.value.capacidad)
        var ram = Number(this.formFiltro.value.ram)
        var laboratorioFiltrado: LaboratorioFiltro = {
          nombreLaboratorio: lab.nombre,
          capacidad: lab.capacidad,
          cumpleCapacidad: (lab.capacidad >= capacidad) ? true : false,
          ram: lab.arregloOrdenador[0].ram,
          cumpleRam: (lab.arregloOrdenador[0].ram >= ram) ? true : false,
          procesador: lab.arregloOrdenador[0].fkProcesador.nombre,
          cumpleProcesador: this.contieneProcesador(lab.arregloOrdenador[0].fkProcesador.id),
          sistemasOperativos: lab.arregloOrdenador[0].arregloOrdenadorSistemaOperativo.map(item => {
            var so = this.auxSistemasOperativos.find(auxitem => { return auxitem.value == item.fkSistemaOperativo.id })
            return so.label
          }),
          sistemasOperativosIncluidos: lab.arregloOrdenador[0].arregloOrdenadorSistemaOperativo.filter(item => {
            return this.formFiltro.value.sistemaOperativo.includes(item.fkSistemaOperativo.id)
          }),
          softwareIncluidos: this.devolverListaSoftwareIncluido(lab.arregloSoftwareLaboratorioCiclo),
          softwareLaboratorio: this.devolverListaNombresSoftwareLaboratorio(lab.arregloSoftwareLaboratorioCiclo),
          sistemasOperativosLaboratorio: lab.arregloOrdenador[0].arregloOrdenadorSistemaOperativo.map(item => { return item.fkSistemaOperativo.nombre }),
          idLaboratorio: lab.id
        }
        this.formFiltro
        return laboratorioFiltrado

      }
    )
    this.listaLaboratorioFiltrada
    this.listaLaboratorioFiltrada.sort((a, b) => {

      return ((Number(a.cumpleCapacidad) - Number(b.cumpleCapacidad))) || ((Number(a.cumpleRam) - Number(b.cumpleRam)));

    }).reverse()

    this.dataSource.data = this.listaLaboratorioFiltrada
  }
  devolverListaSoftwareIncluido(listaSoftwareLaboratorio): string[]{
    return listaSoftwareLaboratorio.filter(
      item=>{
        return this.formFiltro.value.software.includes(item.fkSoftware)
      }
    ).map(
      item=>{
        return item.fkSoftware
      }
    )
  }
  devolverListaNombresSoftwareLaboratorio(listaSoftwareLabor: any[]): any[]{
    return listaSoftwareLabor.map(item=>{
      var software= this.listaSofware.find(soft=>{return soft.id== item.fkSoftware})
      return software.nombre
    })
  }
  seleccionarLaboratorio(element: LaboratorioFiltro) {
    this.dialogRef.close(element)
  }
  contieneProcesador(idProcesador) {
    var contieneProcesador = this.formFiltro.value.procesador.includes(idProcesador)
    return contieneProcesador
  }
  cumpleCondiciones(laboratorio) {
    this.auxProcesador.length
  }
  filtrarCapacidad(minimaCapacidad) {
    this.listaLaboratorios.filter(
      lab => {
        return lab >= minimaCapacidad
      }
    )
  }
  filtrarRam(minimaRam) {
    this.listaLaboratorios.filter(
      lab => {
        return lab >= minimaRam
      }
    )
  }


  inicializarFormFiltro() {
    this.formFiltro = this.fb.group(
      {
        ram: ['', [Validators.required]],
        sistemaOperativo: ['', [Validators.required]],
        procesador: ['', [Validators.required]],
        idOrdenador: [''],
        capacidad: ['', [Validators.required]],
        software: ['', [Validators.required]]
      }
    )
  }

}
