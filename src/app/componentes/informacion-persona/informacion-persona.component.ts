import { Component, OnInit, Inject } from '@angular/core';
import { Persona } from 'src/app/modelos/persona.data';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-informacion-persona',
  templateUrl: './informacion-persona.component.html',
  styleUrls: ['./informacion-persona.component.css']
})
export class InformacionPersonaComponent implements OnInit {
  personaSeleccionado: Persona
  constructor(@Inject(MAT_DIALOG_DATA) private data: any,) {
    
    this.personaSeleccionado = data.informacionPersona
   }

  ngOnInit() {
  }

}
