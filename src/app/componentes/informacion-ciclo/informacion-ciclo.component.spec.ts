import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformacionCicloComponent } from './informacion-ciclo.component';

describe('InformacionCicloComponent', () => {
  let component: InformacionCicloComponent;
  let fixture: ComponentFixture<InformacionCicloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacionCicloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformacionCicloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
