import { Component, OnInit, Inject } from '@angular/core';
import { Ciclo } from 'src/app/modelos/ciclo.data';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-informacion-ciclo',
  templateUrl: './informacion-ciclo.component.html',
  styleUrls: ['./informacion-ciclo.component.css']
})
export class InformacionCicloComponent implements OnInit {
  cicloSeleccionado: Ciclo
  constructor(@Inject(MAT_DIALOG_DATA) private data: any,) {
    
    this.cicloSeleccionado = data.informacionCiclo
   }

  ngOnInit() {
  }

}
