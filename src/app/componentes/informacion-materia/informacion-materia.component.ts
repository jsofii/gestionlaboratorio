import { Component, OnInit, Inject } from '@angular/core';
import { Ciclo } from 'src/app/modelos/ciclo.data';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Materia } from 'src/app/modelos/materia.data';

@Component({
  selector: 'app-informacion-materia',
  templateUrl: './informacion-materia.component.html',
  styleUrls: ['./informacion-materia.component.css']
})
export class InformacionMateriaComponent implements OnInit {
  materiaSeleccionado: Materia
  constructor(@Inject(MAT_DIALOG_DATA) private data: any,) {
    
    this.materiaSeleccionado = data.informacionMateria
   }

  ngOnInit() {
  }

}
