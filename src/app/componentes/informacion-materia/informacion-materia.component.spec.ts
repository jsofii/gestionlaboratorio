import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformacionMateriaComponent } from './informacion-materia.component';

describe('InformacionMateriaComponent', () => {
  let component: InformacionMateriaComponent;
  let fixture: ComponentFixture<InformacionMateriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacionMateriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformacionMateriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
