import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformacionLaboratorioComponent } from './informacion-laboratorio.component';

describe('InformacionLaboratorioComponent', () => {
  let component: InformacionLaboratorioComponent;
  let fixture: ComponentFixture<InformacionLaboratorioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacionLaboratorioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformacionLaboratorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
