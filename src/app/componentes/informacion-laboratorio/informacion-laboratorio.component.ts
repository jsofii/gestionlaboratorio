import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Laboratorio } from 'src/app/modelos/laboratorio.data';

@Component({
  selector: 'app-informacion-laboratorio',
  templateUrl: './informacion-laboratorio.component.html',
  styleUrls: ['./informacion-laboratorio.component.css']
})
export class InformacionLaboratorioComponent implements OnInit {
  laboratrorioSeleccionado: Laboratorio
  constructor(@Inject(MAT_DIALOG_DATA) private data: any,) {
    this.laboratrorioSeleccionado = data.informacionLaboratorio
   }

  ngOnInit() {
  }

}
