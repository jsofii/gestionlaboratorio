import { Component, OnInit } from '@angular/core';
import { ReservaService } from '../reserva/reserva.service';
import { SessionManagerService } from '../servicios/session-manager.service';
import { MatSnackBar } from '@angular/material';
import { SnackbarVistaComponent } from '../snackbar-vista/snackbar-vista.component';
import { Reserva } from '../modelos/reserva.data';

@Component({
  selector: 'app-consulta-horario',
  templateUrl: './consulta-horario.component.html',
  styleUrls: ['./consulta-horario.component.css']
})
export class ConsultaHorarioComponent implements OnInit {

  constructor(private _reservaService:ReservaService, private _sessionManager:SessionManagerService, private _snackBar:MatSnackBar) { }

  horarios:Reserva[]=[];
  ngOnInit() {
    if(this._sessionManager.checkTipoUsuario('Profesor')){
      this.obtenerHorariosDelUsuario(this._sessionManager.obteneridPersona());
    }else{
      this.obtenerTodosLosHorarios()
    }

  }

  columnas = [
    { field: 'fkCiclo.nombre', header: 'Periodo' },
    { field: 'fkPersonaReserva.nombreCompleto', header: 'Profesor' },
    { field: 'fkLaboratorio.nombre', header: 'Laboratorio' },
    { field: 'fkMateria.nombre', header: 'Materia' },
    { field: 'dia', header: 'Día' },
    {field:'fechaInicio',header:'Inicio'},
    {field:'fechaFin',header:'Fin'},

  ];
  
  mostrarMensaje(mensaje: string, type: string = "Info") {

    this._snackBar.openFromComponent(SnackbarVistaComponent, {
      data: { message: mensaje, snackType: type },
      duration: 5000
    });

  }

  obtenerTodosLosHorarios(){
    this._reservaService.obtenerHorarios().subscribe(
      horarios=>{
        this.horarios=horarios.map(reserva=>{
          reserva.fechaInicio=new Date(reserva.fechaInicio.toString());
          reserva.fechaFin=new Date(reserva.fechaFin.toString());
          return reserva;

        });
      },error=>{
        this.mostrarMensaje("Error al cargar los horarios","Error")
        console.log(error);
      }
    )
  }

  obtenerHorariosDelUsuario(idUsuario){
    this._reservaService.obtenerHorariosDelUsuario(idUsuario).subscribe(
      horarios=>{
        this.horarios=horarios.map(reserva=>{
          reserva.fechaInicio=new Date(reserva.fechaInicio.toString());
          reserva.fechaFin=new Date(reserva.fechaFin.toString());
          return reserva;

        });
      },error=>{
        this.mostrarMensaje("Error al cargar las reservas del usuario","Error")
        console.log(error);
      }
    )

  }

}
