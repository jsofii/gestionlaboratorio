import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Materia, Carrera } from '../modelos/materia.data';
import { MateriaService } from './materia.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { SnackbarVistaComponent } from '../snackbar-vista/snackbar-vista.component';
import { AngularFirePerformance } from '@angular/fire/performance';

@Component({
  selector: 'app-materia',
  templateUrl: './materia.component.html',
  styleUrls: ['./materia.component.css']
})
export class MateriaComponent implements OnInit {

  @ViewChild('cerrarModal') cerrarModal: ElementRef
  public formCrearMateria: FormGroup;
  materias: Materia[] = [] as Materia[]
  carreras: Carrera[] = [] as Carrera[]
  carrerasAux=[];
  tituloModalEditar: string;
  mostrarModalCrearP: boolean = false;
  esNuevoRegistro: boolean = false;
  columnas = [
    { field: 'codigo', header: 'Código' },
    { field: 'nombre', header: 'Nombre' },
    { field: 'fkCarrera.nombre', header: 'Carrera' },
    { field: 'estado', header: 'Estado' },
    { field: 'acciones', header: 'Editar' },
    { field: 'activar', header: 'Activar/Desactivar' }
  ];

  constructor(private _angularPerformance: AngularFirePerformance, private _snackBar: MatSnackBar, private _materiaService: MateriaService, private fb: FormBuilder) { }

  ngOnInit() {
    this.cargarDatos()
    this.iniciarFormulario();
  }
  iniciarFormulario() {
    this.formCrearMateria = this.fb.group(
      {
        nombre: ['', [Validators.required]],
        carrera: ['', [Validators.required]],
        codigo: ['', [Validators.required]],
        estado: [],
        idMateria: [],
        nombreCarrera: []
      }
    )
  }
  cargarDatos() {
    this.obtenerMaterias();
    this.obtenerCarreras();

  }
  obtenerCarreras() {
    this._materiaService.listaCarreras().subscribe(
      data => {
        this.carreras = data.filter(item=>{return item.estado=='ACTIVO'});
        this.carrerasAux=this.carreras.map(x=>{return {'label':x.nombre,'value':x.nombre}});
      }
    )
  }

  obtenerMaterias() {
    this._angularPerformance.performance.subscribe(
      performance => {
        const trace = performance.trace('Obtener Materias');
        trace.start();

        this._materiaService.obtener().subscribe(
          materias => {
            this.materias = materias;

            trace.putAttribute('RecuperacionMaterias', 'Registros recuperados:' + this.materias.length);
            trace.stop();
          },
          error => {
            trace.putAttribute('ErrorRecuperacionMateria', error.error);
            trace.stop();
          }
        )
      }
    );

  }

  abrirModalCrear() {
    this.limpiarformCrearMateria();
    this.tituloModalEditar = "Registro de materia"
    this.esNuevoRegistro = true;
  }

  // abrirModalEditar(materia: Materia){
  //   this.tituloModalEditar ="Modificar materia"
  //   this.esNuevoRegistro= false
  //   this.materiaTemporal=materia


  // }
  guardarMateria() {

    if (this.esNuevoRegistro) {
      this.crearMateria();

    } else {
      this.editarMateria();
    }


    this.cerrarModal.nativeElement.click()
  }

  // displayEliminar=false;


  crearMateria() {
    var materia: Materia = {
      nombre: this.formCrearMateria.controls['nombre'].value,
      codigo: this.formCrearMateria.controls['codigo'].value,
      fkCarrera: this.formCrearMateria.controls['carrera'].value,
      estado: "ACTIVO"
    }
    this._angularPerformance.performance.subscribe(
      performance => {
        const trace = performance.trace('Crear Materia');
        trace.start();
        this._materiaService.crear(materia).subscribe(
          materiaCreada => {

            this.materias.push(materiaCreada);
            this.cerrarModal.nativeElement.click()
            this.mostrarMensaje("Materia registrada exitosamente!")
            this.limpiarformCrearMateria();
            trace.putAttribute('CreacionMateria', 'Materia creada:');
            trace.stop();
          },
          error => {
            trace.putAttribute('ErrorRegistroMateria', error.error);
            trace.stop();
            this.mostrarMensaje("Ha surgido un problema con la creación de materia");
          }
        )
      }
    );
  }
  limpiarformCrearMateria() {
    this.formCrearMateria.patchValue({
      nombre: "",
      codigo: "",
      tipoPersona: 1,
      estado: ""

    })
  }
  mostrarMensaje(mensaje: string) {

    this._snackBar.openFromComponent(SnackbarVistaComponent, {
      data: { message: mensaje },
      duration: 3000
    });

  }
  abrirModalEditar(materia: Materia) {
    this.tituloModalEditar = "Modificar materia"
    this.esNuevoRegistro = false
    this.formCrearMateria.patchValue({
      nombre: materia.nombre,
      codigo: materia.codigo,
      carrera: materia.fkCarrera.id,
      estado: materia.estado,
      idMateria: materia.id

    })



  }

  editarMateria() {
    var materia: Materia = {
      id: this.formCrearMateria.controls['idMateria'].value,
      nombre: this.formCrearMateria.controls['nombre'].value,
      fkCarrera: this.formCrearMateria.controls['carrera'].value,
      codigo: this.formCrearMateria.controls['codigo'].value
    }
    this._materiaService.editar(materia).subscribe(
      materiaEditada => {
        let index = this.materias.findIndex(materia => materia.id == materiaEditada.id)
        this.materias[index] = materiaEditada;
        this.mostrarMensaje("Materia editada exitosamente!")
      }
    )

  }
  desactivarMateria(materia: Materia) {
    if (materia.estado == "INACTIVO") {
      materia.estado = "ACTIVO"
    } else {
      materia.estado = "INACTIVO"
    }

    let materiaTemp: Materia = {
      id: materia.id,
      estado: materia.estado
    }

    this._materiaService.editar(materiaTemp).subscribe(
      data => {

        let index = this.materias.findIndex(materia => materia.id == data.id)
        this.materias[index] = data;
        this.mostrarMensaje("Cambios aplicados exitosamente")
      }
    )
  }




}
