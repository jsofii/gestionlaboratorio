import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import {Materia, Carrera} from '../modelos/materia.data';
import { ServicioPrincipalService } from '../servicios/servicio-principal.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { config } from '../controladores/Config';

@Injectable({
  providedIn: 'root'
})
export class MateriaService extends ServicioPrincipalService<Materia>{
  

  private materiasCollection:AngularFirestoreCollection<Materia>
  private materias:Observable<Materia[]>
   
 
  constructor( private _httpClient: HttpClient) {
    super(_httpClient, 'Materia');
   }

  public listaCarreras(){
     return this._httpClient.get<Carrera[]>(config.baseUrl+'/Carrera')
   }

   editarCarrera(carreraTemp: Carrera) {
     return this.httpClient.patch<Carrera>(config.baseUrl+'/Carrera/'+carreraTemp.id,carreraTemp);
   }
 
  registrarCarrera(carreraTemp: Carrera) {
    return this.httpClient.post<Carrera>(config.baseUrl+'/Carrera',carreraTemp);
  }

  
}
