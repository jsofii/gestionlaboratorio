import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from '@angular/fire/firestore'
import {Software} from '../modelos/software.data'
import { ServicioPrincipalService } from '../servicios/servicio-principal.service';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class SoftwareService extends ServicioPrincipalService<Software> {

  constructor(private _httpClient: HttpClient) {
    super(_httpClient, 'Software');
   }

/*
   obtenerSoftware(){
     return this.software
   }
   
   actualizarBD(){
    this.softwareCollection = this._angularFirestore.collection<Software>('software')
    this.software = this.softwareCollection.valueChanges()
   }

   guardarSoftware(software:Software){
     this.softwareCollection.doc(software.id).set(software)
   }

   eliminarSoftware(software: Software) {
     this.softwareCollection.doc(software.id).delete()
   }

   concederAcceso(id:string) {
    this.softwareCollection.doc(id).update({acceso:'Concedido'});
  }

  bloquearAcceso(id: string) {
    this.softwareCollection.doc(id).update({acceso:'Bloqueado'});
  }

  desbloquearAcceso(id: string) {
    this.softwareCollection.doc(id).update({acceso:'Concedido'});
  }*/
}
