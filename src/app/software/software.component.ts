import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SoftwareService } from './software.service';
import {Software} from '../modelos/software.data'
import { v4 as uuid } from 'uuid';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { SnackbarVistaComponent } from '../snackbar-vista/snackbar-vista.component';

@Component({
  selector: 'app-software',
  templateUrl: './software.component.html',
  styleUrls: ['./software.component.css']
})
export class SoftwareComponent implements OnInit {

  @ViewChild('cerrarModal') cerrarModal: ElementRef

  software:Software[]=[]
  public formCrearSoftware: FormGroup;
  tituloModalEditar:string;
  mostrarModalCrearP:boolean=false;
  esNuevoRegistro:boolean=false;
  columnas= [

    { field: 'nombre', header: 'Nombre' },
    { field: 'version', header: 'Versión' },
    { field: 'estado', header: 'Estado' },
    {field:'acciones',header:'Acciones'},
    {field:'activar',header:'Activar/Desactivar'}
  ];

  constructor(private _snackBar: MatSnackBar,private fb: FormBuilder,private _softwareService:SoftwareService) { }

  ngOnInit() {
    this.iniciarFormulario();
    this.obtenerSoftware()
  }
  iniciarFormulario(){
    this.formCrearSoftware= this.fb.group(
      {
        nombre:['', [Validators.required]],
        version:['', [Validators.required]],
        estado:[''],
        id:[''],
        
      
      }
    )
  }
  cargarDatos(){
    this.obtenerSoftware();
    

  }
  obtenerSoftware(){
    this._softwareService.obtener().subscribe(
      listaSoftware=>{
        this.software=listaSoftware
      }
    )
  }
  limpiarRegistro(){
    this.formCrearSoftware.patchValue({
      nombre: "",
      version: ""
    })
  }
 
  abrirModalCrear(){
    this.limpiarRegistro()
    this.tituloModalEditar = "Registro de software"
    this.esNuevoRegistro = true;
  }

  abrirModalEditar(software: Software){
    this.tituloModalEditar ="Modificar software"
    this.esNuevoRegistro= false
   
    this.formCrearSoftware.patchValue({
      nombre: software.nombre,
      version: software.version,
      id: software.id,
      estado: software.estado
    })
  }
  guardarSoftware(){
      if(this.esNuevoRegistro){
        this.crearSoftware();
        
      }else{
        this.editarSoftware();
      }
     
      this.cerrarModal.nativeElement.click()
    
  }
  crearSoftware(){
    var software: Software={
      nombre: this.formCrearSoftware.controls['nombre'].value,
      version: this.formCrearSoftware.controls['version'].value,
      estado: "ACTIVO"
    }

    this._softwareService.crear(software).subscribe(
      softwareCreador => {
        console.log(softwareCreador)
        this.software.push(softwareCreador);
        this.cerrarModal.nativeElement.click()
        this.mostrarMensaje("Software registrada exitosamente!")
        this.limpiarFormCrearsoftware();
       
      }
    )
  }
  editarSoftware(){
    var software: Software={
      id: this.formCrearSoftware.controls['id'].value,
      nombre: this.formCrearSoftware.controls['nombre'].value,
      version: this.formCrearSoftware.controls['version'].value,
     
    }
    this._softwareService.editar(software).subscribe(
      softwareEditado=>{
        let index = this.software.findIndex(software=>software.id==softwareEditado.id)
        this.software[index]=softwareEditado;
        this.mostrarMensaje("Software editado exitosamente!")
      }
    )
  }
  desactivarSoftware(software: Software){
    if(software.estado=="INACTIVO"){
      software.estado="ACTIVO"
    }else{
      software.estado="INACTIVO"
    }

    let softwareTemp:Software={
      id:software.id,
      estado:software.estado
    }
    
    this._softwareService.editar(softwareTemp).subscribe(
      data=>{
        this.mostrarMensaje("Cambios aplicados exitosamente")
        this.obtenerSoftware();
      }
    )
  }
  limpiarFormCrearsoftware(){
    this.formCrearSoftware.patchValue({
      nombre: "",
      version: "",
      estado: ""

    })
  }

  mostrarMensaje(mensaje: string){
  
    this._snackBar.openFromComponent(SnackbarVistaComponent, {
      data: { message: mensaje },
      duration: 3000
    });
  
}

  displayEliminar=false;


  clearSoftware(){

  }

  eliminarSoftware(){
   
    this.clearSoftware()
    this.displayEliminar=false;
    alert('Registro eliminado correctamente')
  }
  

}
