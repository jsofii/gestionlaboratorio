import { Component, OnInit } from '@angular/core';
import { SessionManagerService } from '../servicios/session-manager.service';
import { Router } from '@angular/router';
import { UsuarioService } from '../usuario/usuario.service';
import { MatSnackBar } from '@angular/material';
import { SnackbarVistaComponent } from '../snackbar-vista/snackbar-vista.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  esAdministrador = false;
  esPasante = false;
  esProfesor = false;
  nombreUsuario = '';

  constructor(
    private _usuarioService: UsuarioService,
    private _sessionManager: SessionManagerService,
    private _router: Router,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.checkUsuario();

  }

  checkUsuario() {
    if (this._sessionManager.obtenerID() == undefined) {

      this._router.navigateByUrl('/login').then(
        () => {
          this.mostrarMensaje('Debe iniciar sesión para ingresar al sistema')
        }
      )


    } else {
      this.cargarUsuario();
      if (this._sessionManager.checkTipoUsuario("Profesor")) {
        this.esProfesor = true;
      } else if (this._sessionManager.checkTipoUsuario("Administrador")) {
        this.esAdministrador = true;
      } else if (this._sessionManager.checkTipoUsuario("Pasante")) {
        this.esPasante = true;
      }
    }

  }
  mostrarMensaje(mensaje: string, type: string = "Info") {

    this._snackBar.openFromComponent(SnackbarVistaComponent, {
      data: { message: mensaje, snackType: type },
      duration: 5000
    });

  }

  cargarUsuario() {
    let id = this._sessionManager.obtenerID();
    this._usuarioService.obtenerPorID(id).subscribe(
      usuario => {
        this.nombreUsuario = usuario.fkPersona.nombreCompleto;

      }, error => {
        alert("Error al obtener la información del usuario")
      }
    )
  }

  cerrarSesion() {
    if (confirm("¿Desea cerrar su sesión?")) {
      this._sessionManager.closeSession();
      this._router.navigateByUrl('/login');

    }

  }



}
