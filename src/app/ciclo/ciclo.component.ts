import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { SnackbarVistaComponent } from '../snackbar-vista/snackbar-vista.component';
import { Ciclo } from '../modelos/ciclo.data';
import { MatSnackBar } from '@angular/material';
import { Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CicloService } from './ciclo.service';
import { AngularFirePerformance } from '@angular/fire/performance';

@Component({
  selector: 'app-ciclo',
  templateUrl: './ciclo.component.html',
  styleUrls: ['./ciclo.component.css']
})
export class CicloComponent implements OnInit {

  @ViewChild('cerrarModal') cerrarModal: ElementRef

  ciclo: Ciclo[] = []
  public formCrearCiclo: FormGroup;
  tituloModalEditar: string;
  mostrarModalCrearP: boolean = false;
  esNuevoRegistro: boolean = false;
  columnas = [

    { field: 'nombre', header: 'Nombre' },
    { field: 'fechaInicio', header: 'Fecha Inicio' },
    { field: 'fechaFin', header: 'Fecha Fin' },
    { field: 'estado', header: 'Estado' },
    { field: 'acciones', header: 'Acciones' },
    { field: 'activar', header: 'Activar/Desactivar' }
  ];

  constructor(private _snackBar: MatSnackBar,
    private _angularPerformance: AngularFirePerformance,
    private fb: FormBuilder, private _cicloService: CicloService) { }

  ngOnInit() {
    this.iniciarFormulario();
    this.obtenerCiclo()
  }
  iniciarFormulario() {
    this.formCrearCiclo = this.fb.group(
      {
        nombre: ['', [Validators.required]],
        fechaInicio: ['', [Validators.required]],
        fechaFin: ['', [Validators.required]],
        estado: [''],
        id: [''],


      }
    )
  }
  cargarDatos() {
    this.obtenerCiclo();


  }
  obtenerCiclo() {
    this._angularPerformance.performance.subscribe(
      performance => {
        const trace = performance.trace('Obtener Ciclo');
        trace.start();
        this._cicloService.obtener().subscribe(
          listaCiclo => {
            this.ciclo = listaCiclo
            trace.putAttribute('RecuperacionCiclo', 'Registros recuperados:' + this.ciclo.length);
            trace.stop();
          },
          error => {
            trace.putAttribute('ErrorRecuperacionCiclo', error.error);
            trace.stop();
          }
        )
      }
    )

  }

  abrirModalCrear() {
    this.clearCiclo()
    this.tituloModalEditar = "Registro de periodo"
    this.esNuevoRegistro = true;
  }

  abrirModalEditar(ciclo: Ciclo) {
    this.tituloModalEditar = "Modificar periodo"
    this.esNuevoRegistro = false

    this.formCrearCiclo.patchValue({
      nombre: ciclo.nombre,
      fechaInicio: ciclo.fechaInicio,
      fechaFin: ciclo.fechaFin,
      id: ciclo.id,
      estado: ciclo.estado
    })
  }
  guardarCiclo() {
    if (this.esNuevoRegistro) {
      this.crearCiclo();

    } else {
      this.editarCiclo();
    }

    this.cerrarModal.nativeElement.click()

  }
  crearCiclo() {
    var ciclo: Ciclo = {
      nombre: this.formCrearCiclo.controls['nombre'].value,
      fechaInicio: this.formCrearCiclo.controls['fechaInicio'].value,
      fechaFin: this.formCrearCiclo.controls['fechaFin'].value,
      estado: "ACTIVO"
    }
    this._angularPerformance.performance.subscribe(
      performance=>{
        const trace = performance.trace('Crear Ciclo');
        trace.start();

        this._cicloService.crear(ciclo).subscribe(
          cicloCreador => {

            this.ciclo.push(cicloCreador);
            this.cerrarModal.nativeElement.click()
            this.mostrarMensaje("Periodo registrado exitosamente!")
            this.limpiarFormCrearciclo();
            trace.putAttribute('CreacionCiclo', 'Ciclo creado:');
            trace.stop();
          },
          error=>{
            trace.putAttribute('ErrorRegistroCiclo', error.error);
            trace.stop();
            this.mostrarMensaje("Ha surgido un problema con la creación de periodo");
          }
        )
          }
        )
  }
  editarCiclo() {
    var ciclo: Ciclo = {
      id: this.formCrearCiclo.controls['id'].value,
      nombre: this.formCrearCiclo.controls['nombre'].value,
      fechaInicio: this.formCrearCiclo.controls['fechaInicio'].value,
      fechaFin: this.formCrearCiclo.controls['fechaFin'].value,

    }
    this._cicloService.editar(ciclo).subscribe(
      cicloEditado => {
        let index = this.ciclo.findIndex(ciclo => ciclo.id == cicloEditado.id)
        this.ciclo[index] = cicloEditado;
        this.mostrarMensaje("Periodo editado exitosamente!")
      }
    )
  }
  desactivarCiclo(ciclo: Ciclo) {
    if (ciclo.estado == "INACTIVO") {
      ciclo.estado = "ACTIVO"
    } else {
      ciclo.estado = "INACTIVO"
    }

    let cicloTemp: Ciclo = {
      id: ciclo.id,
      estado: ciclo.estado
    }

    this._cicloService.editar(cicloTemp).subscribe(
      data => {
        this.mostrarMensaje("Cambios aplicados exitosamente")
        this.obtenerCiclo();
      }
    )
  }
  limpiarFormCrearciclo() {
    this.formCrearCiclo.patchValue({
      nombre: "",
      version: "",
      estado: ""

    })
  }

  mostrarMensaje(mensaje: string) {

    this._snackBar.openFromComponent(SnackbarVistaComponent, {
      data: { message: mensaje },
      duration: 3000
    });

  }

  displayEliminar = false;


  clearCiclo() {

  }

  eliminarCiclo() {

    this.clearCiclo()
    this.displayEliminar = false;
    alert('Registro eliminado correctamente')
  }
}
