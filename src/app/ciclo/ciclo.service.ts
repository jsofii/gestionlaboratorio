import { Injectable } from '@angular/core';
import {Persona, TipoPersona} from '../modelos/persona.data'
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ServicioPrincipalService } from '../servicios/servicio-principal.service';
import { map } from 'rxjs/operators';
import { Ciclo } from '../modelos/ciclo.data';
@Injectable({
  providedIn: 'root'
})
export class CicloService extends ServicioPrincipalService<Ciclo> {
  
  constructor(private _httpClient: HttpClient) {
    super(_httpClient, 'Ciclo');
   }

 
 
  
}
