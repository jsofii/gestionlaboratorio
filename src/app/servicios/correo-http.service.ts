import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Correo } from '../modelos/correo.data';
import { config } from '../controladores/Config';

@Injectable({
  providedIn: 'root'
})
export class CorreoHttpService {

  constructor(private _httpClient: HttpClient) { }

  
  enviarCredencialesPorCorreo(correo:string,password:string){
    var correoModel:Correo={
     destinatario:correo,
     asunto:'Bienvenido al Sistema de Gestión de Laboratorios - LABFIS',
     htmlMessage:"<h3>Bienvenido al Sistema de Gestión de Laboratorios de la Facultad de Ingeniería de Sistemas de la Escuela Politécnica Nacional</h3><p>Estimado Usuario, sus credenciales de accesso al sistema son:</p><p><strong>Correo: </strong>"+correo+"</p><p><strong>Contraseña: </strong>"+password+"</p><br><a href='"+config.baseUrl+"' target='_blank'>Ir al Sitio Web</a><br><br><img width='250' src='https://firebasestorage.googleapis.com/v0/b/sistema-de-gestion-labfis.appspot.com/o/labfis-logo.png?alt=media&token=cf508534-e602-409f-b9e2-071fc0733786' alt='Logo LABFIS'>"
   }

   
   var response= this._httpClient.post<Correo>(config.baseUrl+'/enviarCorreo',correoModel)
   return response;
 }

}
