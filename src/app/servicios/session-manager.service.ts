import { Injectable } from '@angular/core';
import { Encriptacion } from '../controladores/Encriptacion';

@Injectable({
  providedIn: 'root'
})
export class SessionManagerService {

    private encriptacion:Encriptacion=new Encriptacion();
   private tipoUsuarioKey='';
   private tipoUsuarioValue='';
   private idUsuarioKey='';
   private idUsuarioValue='';
   public idPersonaLogueado;
   constructor() { 
     this.tipoUsuarioKey=this.encriptacion.encriptar("Tipo");
     this.idUsuarioKey=this.encriptacion.encriptar("idUsuario");
     this.tipoUsuarioValue=localStorage.getItem(this.tipoUsuarioKey);
     this.idUsuarioValue=localStorage.getItem(this.idUsuarioKey);
   }
   guardarIdPersona(idPersona){
     
     localStorage.setItem('puserk', idPersona )
   }
   obteneridPersona(){
     return localStorage.getItem('puserk')
   }

   setSession(tipo:string,id:string|number){
    this.tipoUsuarioValue=this.encriptacion.encriptar(tipo);
    localStorage.setItem(this.tipoUsuarioKey,this.tipoUsuarioValue);
    this.idUsuarioValue=''+id;
    localStorage.setItem(this.idUsuarioKey,''+id);
   }

   closeSession(){
     localStorage.removeItem(this.tipoUsuarioKey);
     localStorage.removeItem(this.idUsuarioKey);
   }

   obtenerID(){
     return this.idUsuarioValue;
   }

   checkTipoUsuario(tipo:string){
    let tipoInput=this.encriptacion.encriptar(tipo);
    if(this.tipoUsuarioValue==tipoInput){
      return true;
    }else{
      return false;
    }
   }


}
