import { Injectable } from '@angular/core';
import { SistemaOperativo } from '../modelos/sistemaOperativo.data';
import { HttpClient } from '@angular/common/http';
import { ServicioPrincipalService } from './servicio-principal.service';

@Injectable({
  providedIn: 'root'
})
export class SistemaOperativoService extends ServicioPrincipalService<SistemaOperativo>{

  constructor(private _httpClient: HttpClient) { 
    super(_httpClient, 'SistemaOperativo');
  }
}
