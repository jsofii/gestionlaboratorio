
import { HttpClient } from '@angular/common/http';
import { Persona } from '../modelos/persona.data';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { config } from '../controladores/Config';

export class ServicioPrincipalService<ClaseEntidad> {
  protected modulo: string;
  
  constructor(protected readonly httpClient:HttpClient,modulo:string) {
    this.modulo = modulo;
   
   }
   obtener():Observable<ClaseEntidad[]> {
    const url = `${config.baseUrl}${'/'+this.modulo}`;
    return this.httpClient.get<ClaseEntidad[]>(url)
    }
    crear(claseEntidad: ClaseEntidad):Observable<ClaseEntidad> {
      const url = `${config.baseUrl}${'/'+this.modulo}`;
      return this.httpClient.post<ClaseEntidad>(url, claseEntidad)
    }
    editar(claseEntidad: ClaseEntidad):Observable<ClaseEntidad> {
      const url = `${config.baseUrl}${'/'+this.modulo}${'/'}${claseEntidad['id']}`;
      return this.httpClient.patch<ClaseEntidad>(url, claseEntidad)
    }
    eliminar(claseEntidad: ClaseEntidad):Observable<ClaseEntidad> {
      const url = `${config.baseUrl}${'/'+this.modulo}${'/'}${claseEntidad['id']}`;
      return this.httpClient.delete<ClaseEntidad>(url, claseEntidad)
    }
    obtenerPorID(id):Observable<ClaseEntidad> {
     const url = `${config.baseUrl}${'/'+this.modulo+'/'+id}`;
     return this.httpClient.get<ClaseEntidad>(url)
     }

}
