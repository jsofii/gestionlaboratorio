import { Component, ViewEncapsulation } from '@angular/core';
import { DayService, WeekService, WorkWeekService, MonthService, AgendaService, MonthAgendaService,TimelineViewsService, TimelineMonthService } from '@syncfusion/ej2-angular-schedule';
import {environment} from '../environments/environment'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [DayService, WeekService, WorkWeekService, MonthService, AgendaService, MonthAgendaService, TimelineViewsService, TimelineMonthService],
  styleUrls: ['./app.component.css'],
//  encapsulation : ViewEncapsulation.None
})
export class AppComponent {
  production=environment.production;
  title = 'GestionLaboratoriosFIS';
}
