import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '../usuario/usuario.service';
import { MatSnackBar } from '@angular/material';
import { SnackbarVistaComponent } from '../snackbar-vista/snackbar-vista.component';
import { Encriptacion } from '../controladores/Encriptacion';

@Component({
  selector: 'app-recuperacion-password',
  templateUrl: './recuperacion-password.component.html',
  styleUrls: ['./recuperacion-password.component.css']
})
export class RecuperacionPasswordComponent implements OnInit {

  
  constructor(private _activatedRoute:ActivatedRoute, 
    private _router:Router,
    private _usuarioService:UsuarioService,
    private _snackBar: MatSnackBar ) { }
  password='';
  passwordConfirm='';
  showPassword=false;
  showPasswordConfirm=false;
  disableRecoverButton=false;
  token='';
  encriptacion:Encriptacion=new Encriptacion();
  ngOnInit() {
    this.token=this._activatedRoute.snapshot.queryParams['token'];
  }

  guardarPassword(){
    if(this.password==this.passwordConfirm){
      this.disableRecoverButton=true;
      let passwordEncrypted=this.encriptacion.encriptar(this.password);
      this._usuarioService.updatePassword(this.token,passwordEncrypted.toString()).subscribe(
        exito=>{
          this.disableRecoverButton=false;
          this.mostrarMensaje(exito.mensaje,"Info");
          if(exito.estado){
            this._router.navigateByUrl('/login')
          }
        },error=>{
          this.disableRecoverButton=false;
          this.mostrarMensaje("Error al actualizar su clave de acceso","Warning");
          console.log(error)
        }
      )
    }else{
      this.mostrarMensaje("Las contraseñas no coinciden","Error");
    }
  }

  mostrarMensaje(mensaje: string, type:string="Info") {

    this._snackBar.openFromComponent(SnackbarVistaComponent, {
      data: { message: mensaje , snackType:  type},
      duration: 5000
    });

  }

}
