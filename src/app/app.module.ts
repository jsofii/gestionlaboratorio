import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID,NgModule } from '@angular/core';
import localeDate from '@angular/common/locales/es-US';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ScheduleModule, RecurrenceEditorModule } from '@syncfusion/ej2-angular-schedule';
import { DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';
import { DateTimePickerModule } from '@syncfusion/ej2-angular-calendars';
import { LaboratorioComponent } from './laboratorio/laboratorio.component';
import { ReservaComponent } from './reserva/reserva.component';
import { HorarioComponent } from './horario/horario.component';
import { SoftwareComponent } from './software/software.component';
import { MateriaComponent } from './materia/materia.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MateriaModule } from './materia/materia.module';
import {
  MatButtonModule, MatFormFieldModule, MatInputModule, MatRippleModule,
  MatOptionModule, MatToolbarModule, MatSidenav, MatSidenavModule, MatNavList, MatListModule, MatSelectModule, MatSnackBarModule, MatSlideToggleModule, MatNativeDateModule, MatCardModule, MatProgressSpinnerModule, MatTabsModule, MatCheckboxModule, MatExpansionModule
} from '@angular/material';
import { HomeComponent } from './home/home.component';
import { GestionComponent } from './gestion/gestion.component';
import { PersonaComponent } from './persona/persona.component';

import {MultiSelectModule} from 'primeng/multiselect';
import { ConsultaHorarioComponent } from './consulta-horario/consulta-horario.component';
import { ConsultaReservaComponent } from './consulta-reserva/consulta-reserva.component';
import { ReporteHorarioComponent } from './reporte-horario/reporte-horario.component';
import { AngularFireModule } from '@angular/fire'
import { AngularFireDatabaseModule } from '@angular/fire/database'
import { AngularFireAuth, AngularFireAuthModule } from '@angular/fire/auth'
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore'
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/tooltip';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { environment } from 'src/environments/environment';

import { MatTableModule } from '@angular/material/table'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatDatepickerModule} from '@angular/material/datepicker';

import { SnackbarVistaComponent } from './snackbar-vista/snackbar-vista.component';
import { CicloComponent } from './ciclo/ciclo.component';
import { AngularFirePerformanceModule } from '@angular/fire/performance';
import { SpinnerOverlayComponent } from './spinner-overlay/spinner-overlay.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SpinnerInterceptor } from './spinner-overlay/spinner-interceptor';


import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatIconModule} from '@angular/material/icon';

import { LoginComponent } from './login/login.component';
import { RecuperacionPasswordComponent } from './recuperacion-password/recuperacion-password.component';
import { MiCuentaComponent } from './mi-cuenta/mi-cuenta.component';
import { FiltroLaboratorioComponent } from './componentes/filtro-laboratorio/filtro-laboratorio/filtro-laboratorio.component';
import { ReporteLogsComponent } from './reporte-logs/reporte-logs.component';
import { InformacionLaboratorioComponent } from './componentes/informacion-laboratorio/informacion-laboratorio.component';
import { InformacionPersonaComponent } from './componentes/informacion-persona/informacion-persona.component';
import { InformacionCicloComponent } from './componentes/informacion-ciclo/informacion-ciclo.component';
import { InformacionMateriaComponent } from './componentes/informacion-materia/informacion-materia.component';
import { CalendarioActividadesComponent } from './calendario-actividades/calendario-actividades.component';
import { registerLocaleData } from '@angular/common';
import { ParametrosComponent } from './parametros/parametros.component';
import { ConfirmacionEliminarEventoComponent } from './componentes/confirmacion-eliminar-evento/confirmacion-eliminar-evento.component';
import { PrincipalComponent } from './principal/principal.component';
registerLocaleData(localeDate);

@NgModule({
  declarations: [

    AppComponent,
    LaboratorioComponent,
    ReservaComponent,
    HorarioComponent,
    SoftwareComponent,
    MateriaComponent,
    UsuarioComponent,
    NavbarComponent,
    HomeComponent,
    GestionComponent,
    PersonaComponent,
    ConsultaHorarioComponent,
    ConsultaReservaComponent,
    ReporteHorarioComponent,
    SnackbarVistaComponent,
    CicloComponent,
    LoginComponent,
    RecuperacionPasswordComponent,
    MiCuentaComponent,
    SpinnerOverlayComponent,
    FiltroLaboratorioComponent,
    ReporteLogsComponent,
    InformacionLaboratorioComponent,
    InformacionPersonaComponent,
    InformacionCicloComponent,
    InformacionMateriaComponent,
    CalendarioActividadesComponent,
    ParametrosComponent,
    ConfirmacionEliminarEventoComponent,
    PrincipalComponent,

  ],
  entryComponents: [
    SnackbarVistaComponent,
    SpinnerOverlayComponent,
    FiltroLaboratorioComponent,
    InformacionLaboratorioComponent,
    InformacionCicloComponent,
    InformacionPersonaComponent,
    InformacionMateriaComponent,
    ConfirmacionEliminarEventoComponent
  ],
  imports: [
    MultiSelectModule,
    MatProgressSpinnerModule,
    DropdownModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatTabsModule,
    MatIconModule,
    MatSnackBarModule,
    MatProgressBarModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatTableModule,
    BrowserModule,
    AppRoutingModule,
    ScheduleModule,
    BrowserAnimationsModule,
    TableModule,
    MateriaModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatOptionModule,
    MatToolbarModule,
    MatSidenavModule,
    TooltipModule,
    FormsModule, MatListModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirePerformanceModule,
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    DialogModule,
    RecurrenceEditorModule,
    HttpClientModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule


  ],
  exports: [SnackbarVistaComponent],
  providers: [AngularFireAuth, AngularFirestore, MatDatepickerModule, { provide: HTTP_INTERCEPTORS, useClass: SpinnerInterceptor, multi: true},{provide: LOCALE_ID, useValue: "es-US"} ],
  bootstrap: [AppComponent]
})
export class AppModule { }
