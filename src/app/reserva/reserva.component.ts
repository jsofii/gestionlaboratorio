import { Component, OnInit, ViewChild, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { EventSettingsModel, DayService, WeekService, WorkWeekService, MonthService, AgendaService, ScheduleComponent, PopupCloseEventArgs, PopupOpenEventArgs, ExcelExportService } from '@syncfusion/ej2-angular-schedule';
import { DropDownList } from '@syncfusion/ej2-dropdowns';
import { createElement } from '@syncfusion/ej2-base';
import { Query, DataManager, ODataAdaptor } from '@syncfusion/ej2-data';

import { applyCategoryColor } from '../reserva/helper';
import { Schedule, EventRenderedArgs, RecurrenceEditor, TimeScaleModel, WorkHoursModel, EJ2Instance, ExportOptions, ActionEventArgs, ToolbarActionArgs, QuickInfoTemplatesModel, CurrentAction } from '@syncfusion/ej2-schedule';
import { L10n, } from '@syncfusion/ej2-base';
import { loadCldr, setCulture } from '@syncfusion/ej2-base';
import { CicloService } from '../ciclo/ciclo.service';
import { Ciclo } from '../modelos/ciclo.data';
import { DateTimePicker, TimePicker } from '@syncfusion/ej2-angular-calendars';
import { MultiSelect, BeforeItemRenderEventArgs } from '@syncfusion/ej2-angular-dropdowns';
import { MateriaService } from '../materia/materia.service';
import { LaboratorioService } from '../laboratorio/laboratorio.service';
import { Laboratorio } from '../modelos/laboratorio.data';
import { MatNativeDateModule, MatSnackBar, MatDialogConfig, MatDialog } from '@angular/material';
import { Materia } from '../modelos/materia.data';
import { Button } from 'protractor';
import { Persona } from '../modelos/persona.data';
import { PersonaService } from '../persona/persona.service';
import { Reserva, LogReserva } from '../modelos/reserva.data';
import { Internationalization } from '@syncfusion/ej2-base';
import { StickyDirection } from '@angular/cdk/table';
import { parse } from 'date-fns';
import { SessionManagerService } from '../servicios/session-manager.service';

import { SnackbarVistaComponent } from '../snackbar-vista/snackbar-vista.component';
import { FiltroLaboratorioComponent } from '../componentes/filtro-laboratorio/filtro-laboratorio/filtro-laboratorio.component';
import { HorarioService } from '../horario/horario.service';
import { AngularFirePerformance } from '@angular/fire/performance';
import { ReservaService } from './reserva.service';

import { ItemModel } from '@syncfusion/ej2-angular-navigations';
import { ConfirmacionEliminarEventoComponent } from '../componentes/confirmacion-eliminar-evento/confirmacion-eliminar-evento.component';
declare var require: any;

L10n.load({
  "es": {
    "schedule": {
      "day": "Día",
      "week": "Semana",
      "workWeek": "Semana laborable",
      "month": "Mes",
      "agenda": "Agenda",
      "weekAgenda": "Semana de Agenda",
      "workWeekAgenda": "Semana de Agenda Laboral",
      "monthAgenda": "Month Agenda",
      "today": "Hoy",
      "noEvents": "No events",
      "emptyContainer": "There are no events scheduled on this day.",
      "allDay": "Todo el día",
      "start": "Inicio",
      "end": "Fin",
      "more": "más",
      "close": "Cerrar",
      "cancel": "Cancelar",
      "noTitle": "(Sin título)",
      "delete": "Eliminar",
      "deleteEvent": "Eliminar Reserva",
      "deleteMultipleEvent": "Eliminar Múltiples Reservas",
      "selectedItems": "Elementos seleccionados",
      "deleteSeries": "Eliminar series",
      "edit": "Editar",
      "editSeries": "Editar",
      "editEvent": "Editar Reserva",
      "createEvent": "Crear",
      "subject": "Asunto",
      "addTitle": "Agregar título",
      "moreDetails": "Más Detalles",
      "save": "Guardar",
      "editContent": "¿Quiere editar esta reserva?",
      "deleteRecurrenceContent": "¿Quiere eliminar esta reserva?",
      "deleteContent": "¿Está seguro de que desea eliminar esta reserva?",
      "deleteMultipleContent": "Are you sure you want to delete the selected events?",
      "newEvent": "Ingreso de reserva",
      "title": "Título",
      "location": "Ubicación",
      "description": "Descripción",
      "timezone": "Zona horaria",
      "startTimezone": "Start Timezone",
      "endTimezone": "End Timezone",
      "repeat": "Se repite",
      "saveButton": "Guardar",
      "cancelButton": "Cancelar",
      "deleteButton": "Eliminar",
      "recurrence": "Recurrencia",
      "wrongPattern": "The recurrence pattern is not valid.",
      "seriesChangeAlert": "¿Los cambios se aplicarán a todas las repeticiones de esta reserva, está de acuerdo?",
      "createError": "The duration of the event must be shorter than how frequently it occurs. Shorten the duration, or change the recurrence pattern in the recurrence event editor.",
      "recurrenceDateValidation": "Some months have fewer than the selected date. For these months, the occurrence will fall on the last date of the month.",
      "sameDayAlert": "Two occurrences of the same event cannot occur on the same day.",
      "editRecurrence": "Editar recurrencia",
      "repeats": "Repeticiones",
      "alert": "Alerta",
      "startEndError": "The selected end date occurs before the start date.",
      "invalidDateError": "The entered date value is invalid.",
      "ok": "Ok",
      "occurrence": "Occurrence",
      "series": "Series",
      "previous": "Anterior",
      "next": "Siguiente",
      "timelineDay": "Timeline Day",
      "timelineWeek": "Timeline Week",
      "timelineWorkWeek": "Timeline Work Week",
      "timelineMonth": "Timeline Month"
    },
    "recurrenceeditor": {
      "none": "Niguno",
      "daily": "Diario",
      "weekly": "Semanalmente",
      "monthly": "Mensualmente",
      "month": "Mes",
      "yearly": "Anualmente",
      "never": "Nunca",
      "until": "Hasta",
      "count": "Número de veces",
      "first": "Primero",
      "second": "Segundo",
      "third": "Tercero",
      "fourth": "Cuarto",
      "last": "Último",
      "repeat": "Repetir",
      "repeatEvery": "Repetir cada",
      "on": "Repetir el día:",
      "end": "Fin",
      "onDay": "Día",
      "days": "Día(s)",
      "weeks": "Semana(s)",
      "months": "Mes(es)",
      "years": "Año(s)",
      "every": "cada",
      "summaryTimes": "vece(s)",
      "summaryOn": "on",
      "summaryUntil": "hasta",
      "summaryRepeat": "Repeticiones",
      "summaryDay": "día(s)",
      "summaryWeek": "semana(s)",
      "summaryMonth": "mes(es)",
      "summaryYear": "año(s)"

    },
    "calendar": {
      "today": "Hoy"
    },
    'formValidator': {
      "required": "Este campo es requerido",
      // "max": "الرجاء إدخال قيمة أقل من أو تساوي {0}",
      // "min": "الرجاء إدخال قيمة أكبر من أو تساوي {0}",
      // "regex": "يرجى إدخال قيمة صحيحة",
      // "tel": "يرجى إدخال رقم هاتف صالح",
      // "pattern": "الرجاء إدخال قيمة نمط صحيح",
      // "equalTo": "يرجى إدخال نص مطابقة صحيح"
    }
  }
});
loadCldr(
  require('cldr-data/supplemental/numberingSystems.json'),
  require('cldr-data/main/es/ca-gregorian.json'),
  require('cldr-data/main/es/currencies.json'),
  require('cldr-data/main/es/numbers.json'),
  require('cldr-data/main/es/timeZoneNames.json')
); // To load the culture based first day of week




@Component({
  selector: 'app-reserva',
  providers: [DayService, WeekService, WorkWeekService, MonthService, AgendaService, ExcelExportService],
  templateUrl: './reserva.component.html',
  styleUrls: ['./reserva.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ReservaComponent implements OnInit {
  @ViewChild("scheduleObj")
  public scheduleObj: ScheduleComponent;
  laboratorioSeleccionado: any;
  cicloSeleccionado: any;
  public selectedDate: Date
  public timeScale: TimeScaleModel = { enable: true, interval: 60, slotCount: 1 };
  public workWeekDays: number[] = [1, 2, 3, 4, 5, 6];
  public scheduleHours: WorkHoursModel = { highlight: false, start: '6:00', end: '22:00' };
  public showWeekend: boolean = false;
  recurElement: HTMLElement
  recurrObject: RecurrenceEditor
  listaTodosHorario: Reserva[] = [] as Reserva[]
  listaHorariosMapeada: any[] = []
  listaHorariosMapeadaSinFiltro: any[] = []
  listaHorariosMapeadaFiltrada: any[] = []
  public esProfesor: boolean = false;

  fin_recurrenceEditor: string = "UNTIL="
  dia_recurrenceEditor: string = "BYDAY="

  listaCiclos: Ciclo[] = [] as Ciclo[]
  listaLaboratorios: Laboratorio[] = [] as Laboratorio[]
  listaMateria: Materia[] = [] as Materia[]
  listaProfesores: Persona[] = [] as Persona[]
  auxListaProfesores: any[] = [] as any[]
  auxListaMaterias: any[] = [] as any[]
  auxListaLaboratorio: any[] = [] as any[]
  auxListaCiclos: any[] = [] as any[]
  reservas: Reserva[] = [] as Reserva[]
  cicloSeleccionado_modelo: Ciclo = {} as Ciclo
  materiaData: { [key: string]: Object }[]
  laboratorioData: { [key: string]: Object }[]
  personaData: { [key: string]: Object }[]
  cicloData: { [key: string]: Object }[]
  cicloTempral: Ciclo = {} as Ciclo
  laboratorioTemporal: Laboratorio = {} as Laboratorio
  recurrenceEditor_Fin: string = ""
  tipo: string = "H"

  source: any[] = [

  ]
  public eventSettings: EventSettingsModel = {

    dataSource: [],
    fields: {
      id: 'Id',
      subject: { name: 'Subject', validation: { required: true } }
    }

  };



  constructor(private _snackBar: MatSnackBar, private _cicloService: CicloService, private _materiaService: MateriaService,
    private _laboratorioService: LaboratorioService, private _personaService: PersonaService,
    private _sessionManagerService: SessionManagerService, private _horarioService: HorarioService,
    public matDialog: MatDialog, private _angularPerformance: AngularFirePerformance,
    public _reservaService: ReservaService) {

  }

  ngOnInit() {

    this.esProfesor = this._sessionManagerService.checkTipoUsuario("Profesor");
    this.cargarDatos()
    //this.scheduleObj.addEventListener('Delete', )
  }
  public onOpenDialog = function (event: any): void {
    // Call the show method to open the Dialog
    this.ejDialog.show();
  };

  public onActionBegin(args: ActionEventArgs & ToolbarActionArgs): void {
    if (args.requestType === 'toolbarItemRendering') {
      const exportItem: ItemModel = {
        align: 'Right', showTextOn: 'Both', prefixIcon: 'e-icon-schedule-excel-export',
        text: 'Excel Export', cssClass: 'e-excel-export', click: this.onExportClick.bind(this)
      };
      args.items.push(exportItem);

    }
  }

  public onExportClick(): void {
    const exportValues: ExportOptions = { fields: ['Id', 'Subject', 'StartTime', 'EndTime', 'Location', 'idUsuario'] };
    this.scheduleObj.exportToExcel(exportValues);
  }
  cargarDatos() {
    this.obtenerCiclos();
    this.obtenerLaboratorios();
    this.obtenerMaterias();
    this.obtenerPersonal();
    this.eventSettings.dataSource = this.source

  }


  obtenerHorario() {
    this._angularPerformance.performance.subscribe(
      performance => {
        const trace = performance.trace('Obtener Reserva');
        trace.start();

        this._horarioService.obtener().subscribe(
          horarios => {
            this.listaTodosHorario = horarios
            this.listaHorariosMapeada = this.listaTodosHorario.filter(
              horario => {
                return horario.fkLaboratorio.id == this.laboratorioSeleccionado
              }
            ).map(
              horario => {

                return this.mapearHorario(horario)
              }
            )
            eventRendered: (args: EventRenderedArgs) => applyCategoryColor(args, this.scheduleObj.currentView)
            this.listaHorariosMapeadaFiltrada = this.listaHorariosMapeada
            this.scheduleObj.eventSettings.dataSource = this.listaHorariosMapeadaFiltrada
            this.listaHorariosMapeadaSinFiltro = this.listaHorariosMapeadaFiltrada
            trace.putAttribute('RecuperacionReserva', 'Registros recuperados:' + this.listaTodosHorario.length);
            trace.stop();
          },
          error => {

            trace.putAttribute('ErrorRecuperacionReserva', error.error);
            trace.stop();
          }
        )
      }
    )
  }
  mapearHorario(horario: Reserva) {
    return {

      Id: horario.id,
      StartTime: horario.fechaInicio,
      EndTime: horario.fechaFin,
      idUsuario: horario.fkPersonaCrea.id,
      laboratorio: horario.fkLaboratorio.id,
      materia: horario.fkMateria?horario.fkMateria['id']:undefined,
      profesor: horario.fkPersonaReserva.id,
      RecurrenceRule: horario.recurrenceRule,
      Subject: horario.subject,
      CategoryColor: horario.tipo == 'H' ? '#0b2136' : '#009688',
      tipo: horario.tipo,



    }
  }
  obtenerCiclos() {
    this._cicloService.obtener().subscribe(
      ciclos => {
        this.listaCiclos = ciclos.sort((a, b) => {

          return (new Date(b.fechaInicio.toString()).getTime() - new Date(a.fechaInicio.toString()).getTime());

        })

        this.auxListaCiclos = this.listaCiclos.map(x => {
          return {
            label: x.nombre,
            value: x.id
          }
        })
        this.cicloTempral = this.auxListaCiclos[0]
        this.cicloSeleccionado = this.auxListaCiclos[0].value
        this.setearFechaCiclo(this.listaCiclos[0].id)
      }
    )
  }
  onChangeCiclo(event: any) {
    this.setearFechaCiclo(event.value)
  }
  onChangeLaboratorio(event: any) {
    this.laboratorioTemporal = this.listaLaboratorios.find(lab => lab.id == event.value)
    this.scheduleObj.eventSettings.dataSource = this.listaTodosHorario.filter(
      horario => {
        return horario.fkLaboratorio.id == event.value
      }
    ).map(
      horario => {

        return this.mapearHorario(horario)
      }
    )
  }
  setearFechaCiclo(idCiclo) {
    this.cicloTempral = this.listaCiclos.find(ciclo => ciclo.id == idCiclo)
    var fechaTemporalFin = this.cicloTempral.fechaFin
    var fechaTemporalInicio = this.cicloTempral.fechaInicio
    this.fin_recurrenceEditor = "UNTIL="
    var fechaFin = new Date(fechaTemporalFin.toString())
    this.selectedDate = new Date(fechaTemporalInicio.toString())


    var dia = fechaFin.getDate() < 10 ? '0' + fechaFin.getDate() : fechaFin.getDate()
    var mes = fechaFin.getMonth() < 10 ? '0' + (fechaFin.getMonth() + 1) : fechaFin.getMonth()

    this.fin_recurrenceEditor = this.fin_recurrenceEditor.concat(fechaFin.getFullYear().toString()).concat(mes.toString()).concat(dia.toString())
    var x = document.getElementById("finCiclo")

  }
  obtenerMaterias() {
    this._materiaService.obtener().subscribe(
      materias => {
        this.listaMateria = materias
        var auxListaMaterias = this.listaMateria.map(x => {
          return {
            nombre: x.nombre,
            id: x.id,
            carrera: x.fkCarrera.nombre
          }
        })
        this.materiaData = auxListaMaterias

      }
    )

  }
  obtenerLaboratorios() {
    this._laboratorioService.obtener().subscribe(
      laboratorios => {
        this.listaLaboratorios = laboratorios.filter(item => { return item.estado == 'ACTIVO' })

        this.laboratorioTemporal = this.listaLaboratorios[0]
        this.auxListaLaboratorio = this.listaLaboratorios.map(x => {
          return {
            label: x.nombre,
            value: x.id
          }
        })

        this.laboratorioSeleccionado = this.auxListaLaboratorio[0].value
        this.obtenerHorario()
        // this.laboratorioData = auxListaLaboratorios
      }
    )
  }
  obtenerPersonal() {
    this._personaService.obtener().subscribe(
      persona => {
        this.listaProfesores = persona
        var auxListaProfesores = this.listaProfesores.map(x => {
          return {
            nombre: x.nombreCompleto,
            id: x.id
          }
        })
        this.personaData = auxListaProfesores
      }
    )
  }

  public oneventRendered(args: EventRenderedArgs): void {
    let categoryColor: string = args.data.CategoryColor as string;
    if (!args.element || !categoryColor) {
      return;
    }
    if (this.scheduleObj.currentView === 'Agenda') {
      (args.element.firstChild as HTMLElement).style.borderLeftColor = categoryColor;
    } else {
      args.element.style.backgroundColor = categoryColor;
    }
  }


  private instance: Internationalization = new Internationalization();
  getTimeString(value: Date): string {
    return this.instance.formatDate(value, { skeleton: 'hm' });
  }
  permiteEditar(tipoReserva: any, idPersonaCrea: any): boolean {
    if (idPersonaCrea) {
      if (tipoReserva == 'H' || (this._sessionManagerService.checkTipoUsuario('Profesor') && idPersonaCrea != this._sessionManagerService.obteneridPersona())) {
        return true
      } else {
        return false
      }
    } else {
      return false
    }

  }

  onPopupOpen(args: PopupOpenEventArgs): void {
    this.seleccionarReservaAEditar(args.data)
    if (args.type == "QuickInfo") {
      var dialogObj = args.element['ej2_instances'][0];
      dialogObj.hide();
      var currentAction: CurrentAction = args.target.classList.contains("e-work-cells") ? "Add" : "EditSeries"  ;
      this.scheduleObj.openEditor(args.data, currentAction);
    
    }

    args.cancel = this.permiteEditar(this.tipoReservaSeleccionada, args.data['idUsuario'])



    if (args.type === 'Editor') {

      let statusElement: HTMLInputElement = args.element.querySelector('#materia') as HTMLInputElement;
      let laboratorio: HTMLInputElement = args.element.querySelector('#laboratorio') as HTMLInputElement;
      let usuarioLogueado: HTMLInputElement = args.element.querySelector('#idUsuario') as HTMLInputElement;
      let cicloSeleccionado: HTMLInputElement = args.element.querySelector('#idCiclo') as HTMLInputElement;
      let statusElementProfesor: HTMLInputElement = args.element.querySelector('#profesor') as HTMLInputElement;

      if (!statusElement.classList.contains('e-dropdownlist')) {
        let dropDownListObject: DropDownList = new DropDownList({
          dataSource: this.materiaData,
          // maps the appropriate column to fields property
          fields: { groupBy: 'carrera', text: 'nombre', value: 'id' },

          //set the placeholder to DropDownList input
          placeholder: "Seleccione una materia",
          allowFiltering: true,



        });
        dropDownListObject.setProperties({ validation: true })



        let dropDownListObjectPersona: DropDownList = new DropDownList({
          dataSource: this.personaData,
          // maps the appropriate column to fields property
          fields: { text: 'nombre', value: 'id' },
          //set the placeholder to DropDownList input
          placeholder: "Seleccione un profesor",
          enabled: !this.esProfesor,
          allowFiltering: true,
          value: this._sessionManagerService.obteneridPersona()



        });

        if (this.laboratorioSeleccionado != null) {
          laboratorio.value = this.laboratorioSeleccionado
        }
        dropDownListObjectPersona.value = this._sessionManagerService.obteneridPersona()

        if (this.scheduleObj.currentAction == 'Add') {
          usuarioLogueado.value = this._sessionManagerService.obteneridPersona()
          statusElementProfesor.value = this._sessionManagerService.obteneridPersona()
        }

        dropDownListObject.appendTo(statusElement);
        if (statusElement.value != '')
          dropDownListObject.text = this.obtenerNombreMateria(statusElement.value)

        if (statusElementProfesor.value != '')
          dropDownListObjectPersona.text = this.obtenerNombreProfesor(statusElementProfesor.value)

        dropDownListObjectPersona.appendTo(statusElementProfesor);

      }
      var finCiclo = this.fin_recurrenceEditor

      if (!args.element.querySelector('.custom-field-row')) {
        let startElement: HTMLInputElement = args.element.querySelector('#StartTime') as HTMLInputElement;
        var minStart=new Date(startElement.value) 
        minStart.setHours(7);
        var maxStart=new Date(startElement.value) 
        maxStart.setHours(21);
        if (!startElement.classList.contains('e-datetimepicker')) {
          var startTimePicker=new DateTimePicker({
            value: new Date(startElement.value) || new Date(),
            enabled: true,
            format: 'HH:mm',
            step: 60,
            showClearButton:false,
            allowEdit:false,
            min:minStart,
            max:maxStart,
            change: function(args){
              var minAux=new Date(args.value)
              minAux.setHours(minAux.getHours()+1)
              endTimePicker.min=minAux
              if(args.element['value'] >= endElement.value){
               endTimePicker.value=minAux
               endElement.value=minAux.toString().substr(16,5)
              }
              
            }


          }, startElement);
        }
        let endElement: HTMLInputElement = args.element.querySelector('#EndTime') as HTMLInputElement;
        var minEnd=new Date(endElement.value) 
        minEnd.setHours(8);
        var maxEnd=new Date(endElement.value) 
        maxEnd.setHours(22);
        if (!endElement.classList.contains('e-datetimepicker')) {
          var endTimePicker=new DateTimePicker({
            value: new Date(endElement.value) || new Date(),
            format: 'HH:mm',
            step: 60,
            showClearButton:false,
            allowEdit:false,
            min:minEnd,
            max:maxEnd,



          },
            endElement);
        }
        startElement.value

        this.recurElement = args.element.querySelector('#RecurrenceEditor');
        var recurrenceRuleTemp = args.data['RecurrenceRule']
        var _this=this;
        if (!this.recurElement.classList.contains('e-recurrenceeditor')) {
          this.recurrObject = new RecurrenceEditor({
            value: recurrenceRuleTemp,
            frequencies: ['none','weekly']


          });
          this.recurrObject.appendTo(this.recurElement);
          (this.scheduleObj.eventWindow as any).recurrenceEditor = this.recurrObject;
        }

        document.getElementById('RecurrenceEditor').style.display = (this.scheduleObj.currentAction == "EditOccurrence") ? 'none' : 'block';
        cicloSeleccionado.value = this.cicloSeleccionado.toString()
        setCulture('es')


      }
    }
  }

  devuelveDiaParaRecurrencia(fechaTemporal: Date) {
    switch (fechaTemporal.getDay()) {
      case 1:
        return 'MO';
      case 2:
        return 'TU';
      case 3:
        return 'WE';
      case 4:
        return 'TH';
      case 5:
        return 'FR';
      case 6:
        return 'SA';
      case 7:
        return 'SU';

    }
  }

  abrirModalFiltroLaboratorio() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.id = "filtro-laboratorio"
    dialogConfig.height = "350px";
    dialogConfig.width = "600px";

    const modalDialog = this.matDialog.open(FiltroLaboratorioComponent, {
      data: {
        listaLaboratorios: this.listaLaboratorios
      }
    })
    modalDialog.afterClosed().subscribe(
      data => {
        var laboratorioSeleccionado = data
        this.laboratorioSeleccionado = data.idLaboratorio
        var lab = {
          value: data.idLaboratorio,
          label: data.nombreLaboratorio
        }
        this.onChangeLaboratorio(lab)
        this.mostrarMensaje("Laboratorio " + data.nombreLaboratorio + " seleccionado")
      }
    )

  }


  obtenerNombreMateria(id: string) {
    var idTemp: number = Number(id)
    var materiaTemporal = this.listaMateria.find(x => x.id == idTemp)
    return materiaTemporal.nombre
  }
  obtenerNombreCiclo(id: string) {

    var idTemp: number = Number(id)
    var cicloTemporal = this.listaCiclos.find(x => x.id == idTemp)
    return cicloTemporal.nombre
  }
  obtenerNombreProfesor(id: string) {

    var idTemp: number = Number(id)
    var profesorTemporal = this.listaProfesores.find(x => x.id == idTemp)
    return profesorTemporal.nombreCompleto
  }

  guardarReserva(args) {
    var horarioTemporal: Reserva = {
      fechaInicio: args.StartTime,
      fechaFin: args.EndTime,
      fkCiclo: this.cicloSeleccionado,
      fkLaboratorio: this.laboratorioSeleccionado,
      fkPersonaCrea: this._sessionManagerService.obteneridPersona(),
      fkPersonaReserva: args.profesor,
      fkMateria: args.materia,
      subject: args.Subject,
      recurrenceRule: args.RecurrenceRule == null ? "" : args.RecurrenceRule,
      tipo: 'R'
    }
    this._horarioService.crear(horarioTemporal).subscribe(
      horarioCreado => {

        this.guardarLogReservaCrear(horarioCreado)
        this.guardarReservaLocal(horarioCreado)
        this.onChangeLaboratorio({ value: this.laboratorioSeleccionado })
        this.mostrarMensaje('Reserva registrada correctamente')
        this.scheduleObj.saveEvent(this.mapearHorario(horarioCreado), args.id)
      }
    )
  }
  guardarLogReservaCrear(_reserva: Reserva) {
    var logReserva: LogReserva = {
      accion: "CREAR",
      actual: JSON.stringify(_reserva),
      anterior: "",
      idReserva: _reserva.id,
      idUsuario: Number(this._sessionManagerService.obteneridPersona())
    }
    this._reservaService.guardarLogReserva(logReserva).subscribe(
      data => {

      }
    )

  }
  guardarLogReservaEliminar(_reserva: Reserva) {
    var logReserva: LogReserva = {
      accion: "ELIMINAR",
      actual: "",
      anterior: JSON.stringify(_reserva),
      idReserva: _reserva.id,
      idUsuario: Number(this._sessionManagerService.obteneridPersona())
    }
    this._reservaService.guardarLogReserva(logReserva).subscribe(
      data => {

      }
    )

  }
  guardarReservaLocal(horario: Reserva) {
    this.listaTodosHorario.push(horario)
    //this.listaHorariosMapeadaSinFiltro.push(this.mapearHorario(horario))
  }
  editarReservaLocal(horario: Reserva) {
    var indexHorario = this.listaTodosHorario.findIndex(item => item.id == horario.id)
    this.listaTodosHorario[indexHorario] = horario
  }

  editarReserva(args) {

    var horarioTemporal: Reserva = {
      id: this.idReservaSeleccionada,
      fechaInicio: args.StartTime,
      fechaFin: args.EndTime,
      fkCiclo: this.cicloSeleccionado,
      fkLaboratorio: this.laboratorioSeleccionado,
      fkPersonaCrea: this._sessionManagerService.obteneridPersona(),
      fkPersonaReserva: args.profesor,
      fkMateria: args.materia,
      subject: args.Subject,
      recurrenceRule: args.RecurrenceRule == null ? "" : args.RecurrenceRule,
      tipo: 'R'
    }
    this._reservaService.editarReserva(horarioTemporal, Number(this._sessionManagerService.obteneridPersona())).subscribe(
      horarioEditado => {
        this.editarReservaLocal(horarioEditado)
        this.mostrarMensaje('Reserva editada correctamente')
      }
    )
  }
  profesor(idPersona) {
    var nombreProfesor = this.listaProfesores.find(x => { return x.id == idPersona })
    return nombreProfesor.nombreCompleto
  }
  eliminarReserva(args) {
    var horarioTemporal: Reserva = {
      id: this.idReservaSeleccionada,
      fechaInicio: args.StartTime,
      fechaFin: args.EndTime,
      fkCiclo: this.cicloSeleccionado,
      fkLaboratorio: this.laboratorioSeleccionado,
      fkPersonaCrea: this._sessionManagerService.obteneridPersona(),
      fkPersonaReserva: args.profesor,
      fkMateria: args.materia,
      subject: args.Subject,
      recurrenceRule: args.RecurrenceRule,
      tipo: 'R'
    }
    this._horarioService.eliminar(horarioTemporal).subscribe(
      horario => {
        this.eliminarResevaLocal(horario)
        this.guardarLogReservaEliminar(horario)
        this.mostrarMensaje('Reserva eliminada correctamente')
      }
    )
  }
  eliminarResevaLocal(horario: Reserva) {
    var indexHorario = this.listaTodosHorario.findIndex(item => item.id == horario.id)
    this.listaTodosHorario.splice(indexHorario, 1)
    this.scheduleObj.deleteEvent(horario.id)
  }
  tipoReservaSeleccionada
  idReservaSeleccionada: any
  seleccionarReservaAEditar(args) {
    if (args.Id != undefined)
      this.idReservaSeleccionada = args.Id
    this.tipoReservaSeleccionada = args.tipo
  }
  mostrarMensaje(mensaje: string) {

    this._snackBar.openFromComponent(SnackbarVistaComponent, {
      data: { message: mensaje },
      duration: 3000
    });

  }

  actionComplete(args: ActionEventArgs) {
    switch (args.requestType) {
      case 'eventCreated':
        this.guardarReserva(args.addedRecords[0]);
        break;
      case 'eventChanged':
        this.editarReserva(args.changedRecords[0]);
        break;
      case 'eventRemoved':
        this.eliminarReserva(args.deletedRecords[0]);
        break;
    }
  }




}
