import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ServicioPrincipalService } from '../servicios/servicio-principal.service';
import { Reserva, LogReserva } from '../modelos/reserva.data';
import { config } from '../controladores/Config';

@Injectable({
  providedIn: 'root'
})
export class ReservaService  extends ServicioPrincipalService<Reserva> {

  constructor(private _httpClient: HttpClient) {
    super(_httpClient, 'Reservas');
   }

   guardarLogReserva(reserva: LogReserva){
    return this.httpClient.post<LogReserva>(config.baseUrl+'/logreserva', reserva)
   }

   editarReserva(reserva: Reserva, _idUsuario: number){
     return this.httpClient.patch<any>(config.baseUrl+'/editarReserva?idUsuario='+_idUsuario, reserva)
   }

   obtenerLogReserva(idReserva: number){
    return this.httpClient.get<LogReserva[]>(config.baseUrl+'/logreserva')
   }

   obtenerReservas(){
    return this.httpClient.get<Reserva[]>(config.baseUrl+'/reservas?tipo=R');
   }

   obtenerReservasDelUsuario(idUsuario) {
    return this.httpClient.get<Reserva[]>(config.baseUrl+'/reservas?tipo=R&fkPersonaReserva='+idUsuario);
   }
   
  obtenerHorarios() {
    return this.httpClient.get<Reserva[]>(config.baseUrl+'/reservas?tipo=H');
  }
  obtenerHorariosDelUsuario(idUsuario: any) {
    return this.httpClient.get<Reserva[]>(config.baseUrl+'/reservas?tipo=H&fkPersonaReserva='+idUsuario);
  }

}
