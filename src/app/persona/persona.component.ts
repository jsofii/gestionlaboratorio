import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { PersonaService } from './persona.service';
import { Persona, TipoPersona } from '../modelos/persona.data';
import { v4 as uuid } from 'uuid';
import { ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import {AngularFirePerformance} from '../../../node_modules/@angular/fire/performance'
import { MatSnackBar } from '@angular/material';

import { SnackbarVistaComponent } from '../snackbar-vista/snackbar-vista.component';


@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css'],
  encapsulation : ViewEncapsulation.None
})
export class PersonaComponent implements OnInit {

  @ViewChild('cerrarModal') cerrarModal: ElementRef
  

  personas: Persona[] = [];
  listaTiposPersona: TipoPersona[] = [] as TipoPersona[]
  auxListaTiposPersona: any[] = [] as any[]

  public formCrearPersona: FormGroup;
  tituloModalEditar: string;
  mostrarModalCrearP: boolean = false;
  esNuevoRegistro: boolean = false;
  columnas = [

    { field: 'nombreCompleto', header: 'Nombre Completo' },
    { field: 'correo', header: 'Correo' },
    { field: 'fkTipoPersona.nombre', header: 'Rol' },
    { field: 'estado', header: 'Estado' },
    { field: 'acciones', header: 'Acciones' },
    { field: 'acciones', header: 'Activar/Desactivar' }
  ];

  constructor(private _angularPerformance:AngularFirePerformance,
    private _snackBar: MatSnackBar,
    private fb: FormBuilder,
    private _personaService: PersonaService) { }
  displayedColumns: string[] = ['nombre'];
  emailRegEx = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';

  ngOnInit() {
    this.cargarDatos()
    this.iniciarFormulario()
  }
  iniciarFormulario(){
    this.formCrearPersona= this.fb.group(
      {
        nombreCompleto:['', [Validators.required]],
        correo:['', [ Validators.required, Validators.pattern(this.emailRegEx)]],
        tipoPersona:[],
        estado: [''],
        idPersona: []
      }
    )
  }

  cargarDatos(){
    this.obtenerPersonas();
    this.obtenerlistaTiposPersonas();
  

  }
  obtenerlistaTiposPersonas(){
    this._personaService.obtenerlistaTiposPersonas().subscribe(
      listaTiposPersona=>{
        this.listaTiposPersona=listaTiposPersona
        this.auxListaTiposPersona=this.listaTiposPersona.map(x=>{
          return { 
            label: x.nombre,
            value: x.id
          }
        })
      }
    )
  }


   obtenerPersonas() {
      this._angularPerformance.performance.subscribe(
      performance=>{
        const trace = performance.trace('Obtener Nomina');
        trace.start();
     
        this._personaService.obtener().subscribe(
          personas => {
            this.personas = personas;
            trace.putAttribute('RecuperacionNomina', 'Registros recuperados:'+ this.personas.length);
            trace.stop();
          },
          error=>{
            trace.putAttribute('ErrorRecuperacionNomina', error.error);
            trace.stop();
          }
        )
      }
    );
  }
  crearPersona(){
    var persona: Persona={
      nombreCompleto: this.formCrearPersona.controls['nombreCompleto'].value,
      fkTipoPersona: this.formCrearPersona.controls['tipoPersona'].value,
      correo: this.formCrearPersona.controls['correo'].value,
      estado: "ACTIVO"
    }
    this._angularPerformance.performance.subscribe(
      performance=>{
        const trace = performance.trace('Crear Nomina');
        trace.start();
        this._personaService.crear(persona).subscribe(
          personaCreada => {
            console.log(personaCreada)
            this.personas.push(personaCreada);
            this.cerrarModal.nativeElement.click()
            this.mostrarMensaje("Persona registrada exitosamente!")
            this.limpiarFormCrearPersona();
            trace.putAttribute('CreacionNomina', 'Persona creada:');
            trace.stop();
          },
          error=>{
            trace.putAttribute('ErrorRegistroPersona', error.error);
            trace.stop();
            this.mostrarMensaje("Ha surgido un problema con la creación de nómina");
          }
        )
      }
      );
  }

  editarPersona(){
    var persona: Persona={
      id: this.formCrearPersona.controls['idPersona'].value,
      nombreCompleto: this.formCrearPersona.controls['nombreCompleto'].value,
      fkTipoPersona: this.formCrearPersona.controls['tipoPersona'].value,
      correo: this.formCrearPersona.controls['correo'].value
    }
    this._personaService.editar(persona).subscribe(
      persondaEditada=>{
        let index = this.personas.findIndex(persona=>persona.id==persondaEditada.id)
        this.personas[index]=persondaEditada;
        this.mostrarMensaje("Persona editada exitosamente!")
      }
    )

  }


  limpiarFormCrearPersona(){
    this.formCrearPersona.patchValue({
      nombreCompleto: "",
      correo: "",
      tipoPersona: 1,
      estado: ""

    })
  }
  abrirModalCrear(){
    this.limpiarFormCrearPersona()
    this.tituloModalEditar = "Registro de persona"
    this.esNuevoRegistro = true;
  }

  mostrarMensaje(mensaje: string){
  
      this._snackBar.openFromComponent(SnackbarVistaComponent, {
        data: { message: mensaje },
        duration: 3000
      });
    
  }

  abrirModalEditar(persona: Persona){
    this.tituloModalEditar ="Modificar persona"
    this.esNuevoRegistro= false
    this.formCrearPersona.patchValue({
      nombreCompleto: persona.nombreCompleto,
      correo: persona.correo,
      tipoPersona: persona.fkTipoPersona.id,
      estado: persona.estado,
      idPersona: persona.id

    })

 

  }
  guardarPersona(){

    if(this.esNuevoRegistro){
      this.crearPersona();
      
    }else{
      this.editarPersona();
    }
   
  
    this.cerrarModal.nativeElement.click()
  }


  desactivarPersona(persona: Persona){
    if(persona.estado=="INACTIVO"){
      persona.estado="ACTIVO"
    }else{
      persona.estado="INACTIVO"
    }

    let personaTemp:Persona={
      id:persona.id,
      estado:persona.estado
    }
    
    this._personaService.editar(personaTemp).subscribe(
      personaActualizada=>{
        this.mostrarMensaje("Cambios aplicados exitosamente");
        let index=this.personas.findIndex(persona=>persona.id==personaActualizada.id);
        this.personas[index]=personaActualizada;
      },error=>{
        //AGREGAR LOG DE ERROR EDITAR PERSONA
        this.mostrarMensaje("Error al editar los datos de la persona");
      }
    )
  }

  displayEliminar = false;



}
