import { Injectable } from '@angular/core';
import {Persona, TipoPersona} from '../modelos/persona.data'
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ServicioPrincipalService } from '../servicios/servicio-principal.service';
import { map } from 'rxjs/operators';
import { config } from '../controladores/Config';
@Injectable({
  providedIn: 'root'
})
export class PersonaService extends ServicioPrincipalService<Persona> {
  
  


   
  constructor(private _httpClient: HttpClient) {
    super(_httpClient, 'Persona');
   }

 
   obtenerlistaTiposPersonas(){
     var listaTiposPersona= this._httpClient.get<TipoPersona[]>(config.baseUrl+'/TipoPersona')
     return listaTiposPersona;
   }

   obtenerPorCorreo(correo){
    var listaPersonas= this._httpClient.get<Persona[]>(config.baseUrl+'/Persona?correo='+correo+"&estado=ACTIVO")
    return listaPersonas;
   }
  
}
