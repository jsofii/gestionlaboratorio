import { TestBed } from '@angular/core/testing';

import { ReporteLogsService } from './reporte-logs.service';

describe('ReporteLogsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReporteLogsService = TestBed.get(ReporteLogsService);
    expect(service).toBeTruthy();
  });
});
