import { Component, OnInit } from '@angular/core';
import { ReporteLogsService } from './reporte-logs.service';
import { LogReserva, Reserva } from '../modelos/reserva.data';
import { HorarioService } from '../horario/horario.service';
import { UsuarioService } from '../usuario/usuario.service';
import { Usuario, Persona } from '../modelos/persona.data';
import { LaboratorioService } from '../laboratorio/laboratorio.service';
import { Laboratorio } from '../modelos/laboratorio.data';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { InformacionLaboratorioComponent } from '../componentes/informacion-laboratorio/informacion-laboratorio.component';
import { Ciclo } from '../modelos/ciclo.data';
import { InformacionCicloComponent } from '../componentes/informacion-ciclo/informacion-ciclo.component';
import { Materia } from '../modelos/materia.data';
import { InformacionMateriaComponent } from '../componentes/informacion-materia/informacion-materia.component';
import { InformacionPersonaComponent } from '../componentes/informacion-persona/informacion-persona.component';
import * as XLSX from 'xlsx';
import { PersonaService } from '../persona/persona.service';
@Component({
  selector: 'app-reporte-logs',
  templateUrl: './reporte-logs.component.html',
  styleUrls: ['./reporte-logs.component.css']
})
export class ReporteLogsComponent implements OnInit {
  columnas = [
    { field: 'accion', header: 'Acción', width: '10%' },
    { field: 'tipo', header: 'Tipo', width: '10%' },
    { field: 'createdAt', header: 'Fecha registro', width: '10%' },
    { field: 'anterior', header: 'Anterior', width: '30%' },
    { field: 'actual', header: 'Actual', width: '30%' },
    { field: 'idReserva', header: 'ID reserva', width: '10%' },
    { field: 'idUsuario.nombreCompleto', header: 'Usuario responsable', width: '10%' }
  ];
  listaLogsReservas: LogReserva[] = [] as LogReserva[]
  listaLogsReservasOriginal: LogReserva[] = [] as LogReserva[]
  listaReservas: Reserva[] = [] as Reserva[]
  listaPErsona: Usuario[] = [] as Usuario[]
  listaLaboratorios: Laboratorio[] = [] as Laboratorio[]
  listaLogsReservasMapeada: LogReserva[] = [] as LogReserva[]

  constructor(private _servicioLogsReserva: ReporteLogsService, private _horarioService: HorarioService,
    private personaService: PersonaService, private _laboratorioService: LaboratorioService,
    
    public matDialog: MatDialog) { }

  ngOnInit() {
    this.cargaInicial()
  }
  cargaInicial() {
    this.obtenerLogsReservas()


  }
  abrirModalInformacionLaboratorio(laboratorio: Laboratorio) {

    const modalDialog = this.matDialog.open(InformacionLaboratorioComponent, {
      data: {
        informacionLaboratorio: laboratorio
      }
    })

  }
  abrirModalInformacionCiclo(ciclo: Ciclo) {
    const modalDialog = this.matDialog.open(InformacionCicloComponent, {
      data: {
        informacionCiclo: ciclo
      }
    })
  }
  abrirModalInformacionMateria(materia: Materia) {
    const modalDialog = this.matDialog.open(InformacionMateriaComponent, {
      data: {
        informacionMateria: materia
      }
    })
  }
  abrirModalInformacionPersona(persona: Persona) {
    const modalDialog = this.matDialog.open(InformacionPersonaComponent, {
      data: {
        informacionPersona: persona
      }
    })
  }
  obtenerLogsReservas() {
    this._servicioLogsReserva.devuelveLogsReserva().subscribe(
      logsRserva => {
        this.listaLogsReservas = logsRserva
        this.obtenerUsuarios();
      }
    )
  }
  obtenerReservas() {
    this._horarioService.obtener().subscribe(
      listaReservas => {
        this.listaReservas = listaReservas
        this.mapearListaLogsReserva()

      }
    )
  }
  obtenerUsuarios() {
    this.personaService.obtener().subscribe(
      listaUsrs => {
        this.listaPErsona = listaUsrs
        this.obtenerReservas();
      }
    )
  }


  mapearListaLogsReserva() {
    
    this.listaLogsReservasMapeada = this.listaLogsReservas.map(
      log => {


        var logTemp: LogReserva = log
        if (logTemp.anterior != '') {

          logTemp.anterior = JSON.parse(logTemp.anterior.toString())

        }

        if (logTemp.actual != '') {

          logTemp.actual = JSON.parse(logTemp.actual.toString())
        }
        logTemp.idUsuario = this.listaPErsona.find(item=>{return item.id == logTemp.idUsuario})
        return logTemp
      }
    )
   

  }
  parsearFecha(fechaEntero: number) {
    return new Date(fechaEntero)
  }
  parsearReserva(jsonReserva: string) {
    var reservaParseada = ''
    if (jsonReserva != '')
      reservaParseada = JSON.parse(jsonReserva)
    reservaParseada
    return reservaParseada
  }
  fileName = 'ExcelSheet.xlsx';
  parsearAJson(objeto: any){
    return JSON.stringify(objeto)
  }

  exportexcel(): void {
    /* table id is passed over here */
    let element = document.getElementById('excel-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);

  }


}
