import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { LogReserva } from '../modelos/reserva.data';
import { HttpClient } from '@angular/common/http';
import { config } from '../controladores/Config';

@Injectable({
  providedIn: 'root'
})
export class ReporteLogsService {

  constructor(private _httpClient: HttpClient) { }

  devuelveLogsReserva(){
    return this._httpClient.get<LogReserva[]>(config.baseUrl+'/logreserva')
  }
}
