import { Injectable } from '@angular/core';
import { ServicioPrincipalService } from '../servicios/servicio-principal.service';
import { Reserva } from '../modelos/reserva.data';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HorarioService extends ServicioPrincipalService<Reserva> {

  constructor(private _httpClient: HttpClient) {
    super(_httpClient, 'Reservas');
   }

}
