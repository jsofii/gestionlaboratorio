import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { EventSettingsModel, DayService, WeekService, WorkWeekService, MonthService, AgendaService, ScheduleComponent, PopupCloseEventArgs, PopupOpenEventArgs } from '@syncfusion/ej2-angular-schedule';
import { DropDownList } from '@syncfusion/ej2-dropdowns';
import { createElement } from '@syncfusion/ej2-base';
import { Query, DataManager, ODataAdaptor } from '@syncfusion/ej2-data';

import { applyCategoryColor } from '../reserva/helper';
import { Schedule, EventRenderedArgs, RecurrenceEditor, TimeScaleModel, WorkHoursModel, EJ2Instance, ActionEventArgs, CurrentAction } from '@syncfusion/ej2-schedule';
import { L10n, } from '@syncfusion/ej2-base';
import { loadCldr, setCulture } from '@syncfusion/ej2-base';
import { CicloService } from '../ciclo/ciclo.service';
import { Ciclo } from '../modelos/ciclo.data';
import { DateTimePicker, TimePicker } from '@syncfusion/ej2-angular-calendars';
import { MultiSelect, BeforeItemRenderEventArgs } from '@syncfusion/ej2-angular-dropdowns';
import { MateriaService } from '../materia/materia.service';
import { LaboratorioService } from '../laboratorio/laboratorio.service';
import { Laboratorio } from '../modelos/laboratorio.data';
import { MatNativeDateModule, MatSnackBar, MatDialogConfig, MatDialog } from '@angular/material';
import { Materia } from '../modelos/materia.data';
import { Button } from 'protractor';
import { Persona } from '../modelos/persona.data';
import { PersonaService } from '../persona/persona.service';
import { Reserva, LogReserva } from '../modelos/reserva.data';
import { Internationalization } from '@syncfusion/ej2-base';
import { StickyDirection } from '@angular/cdk/table';
import { parse } from 'date-fns';
import { SessionManagerService } from '../servicios/session-manager.service';
import { HorarioService } from './horario.service';
import { SnackbarVistaComponent } from '../snackbar-vista/snackbar-vista.component';
import { FiltroLaboratorioComponent } from '../componentes/filtro-laboratorio/filtro-laboratorio/filtro-laboratorio.component';
import { AngularFirePerformance } from '@angular/fire/performance';
import { ReservaService } from '../reserva/reserva.service';
import { ArrayType } from '@angular/compiler';
import { ConfirmacionEliminarEventoComponent } from '../componentes/confirmacion-eliminar-evento/confirmacion-eliminar-evento.component';
import { SpinnerOverlayService } from '../spinner-overlay/spinner-overlay.service';
import { start } from 'repl';


declare var require: any;

loadCldr(
  require('cldr-data/supplemental/numberingSystems.json'),
  require('cldr-data/main/es/ca-gregorian.json'),
  require('cldr-data/main/es/currencies.json'),
  require('cldr-data/main/es/numbers.json'),
  require('cldr-data/main/es/timeZoneNames.json')
); // To load the culture based first day of week

@Component({
  selector: 'app-horario',
  templateUrl: './horario.component.html',
  styleUrls: ['./horario.component.css'],
  providers: [WeekService, MonthService, AgendaService],
})
export class HorarioComponent implements OnInit, AfterViewInit {

  @ViewChild("scheduleObj")
  public scheduleObj: ScheduleComponent;
  laboratorioSeleccionado: any;
  cicloSeleccionado: any;
  public selectedDate: Date
  public timeScale: TimeScaleModel = { enable: true, interval: 60, slotCount: 1 };
  public workWeekDays: number[] = [1, 2, 3, 4, 5, 6];
  public scheduleHours: WorkHoursModel = { highlight: false, start: '6:00', end: '22:00' };
  public showWeekend: boolean = false;
  recurElement: HTMLElement
  recurrObject: RecurrenceEditor
  listaTodosHorario: Reserva[] = [] as Reserva[]
  listaHorariosMapeada: any[] = []
  listaHorariosMapeadaSinFiltro: any[] = []
  listaHorariosMapeadaFiltrada: any[] = []

  fin_recurrenceEditor: string = "UNTIL="
  dia_recurrenceEditor: string = "BYDAY="

  listaCiclos: Ciclo[] = [] as Ciclo[]
  listaLaboratorios: Laboratorio[] = [] as Laboratorio[]
  listaMateria: Materia[] = [] as Materia[]
  listaProfesores: Persona[] = [] as Persona[]
  auxListaProfesores: any[] = [] as any[]
  auxListaMaterias: any[] = [] as any[]
  auxListaLaboratorio: any[] = [] as any[]
  auxListaCiclos: any[] = [] as any[]
  reservas: Reserva[] = [] as Reserva[]
  cicloSeleccionado_modelo: Ciclo = {} as Ciclo
  materiaData: { [key: string]: Object }[]
  laboratorioData: { [key: string]: Object }[]
  personaData: { [key: string]: Object }[]
  cicloData: { [key: string]: Object }[]
  recurrenceEditor_Fin: string = ""
  cicloTempral: Ciclo = {} as Ciclo
  laboratorioTemporal: Laboratorio = {} as Laboratorio
  tipo: string = "H"
  alertaHoraInicioMenor: boolean = false;
  alertaHoraInicioMayor: boolean = false;
  alertaHoraFinMenor: boolean = false;
  alertaHoraFinMayor: boolean = false;
  source: any[] = [

  ]
  public eventSettings: EventSettingsModel = {

    dataSource: [],
    fields: {
      id: 'Id',
      subject: { name: 'Subject', validation: { required: true } }
    }

  };



  constructor(private _snackBar: MatSnackBar, private _cicloService: CicloService, private _materiaService: MateriaService,
    private _laboratorioService: LaboratorioService, private _personaService: PersonaService,
    private _sessionManagerService: SessionManagerService, private _horarioService: HorarioService,
    public matDialog: MatDialog, private _angularPerformance: AngularFirePerformance,
    public _reservaService: ReservaService, private readonly spinnerOverlayService: SpinnerOverlayService) {

  }

  ngOnInit() {
   
    L10n.load({
      "es": {
        "schedule": {
          "day": "Día",
          "week": "Semana",
          "workWeek": "Semana laborable",
          "month": "Mes",
          "agenda": "Agenda",
          "weekAgenda": "Semana de Agenda",
          "workWeekAgenda": "Semana de Agenda Laboral",
          "monthAgenda": "Month Agenda",
          "today": "Hoy",
          "noEvents": "No events",
          "emptyContainer": "There are no events scheduled on this day.",
          "allDay": "Todo el día",
          "start": "Inicio",
          "end": "Fin",
          "more": "más",
          "close": "Cerrar",
          "no": "Si",
          "cancel": "Cancelar",
          "noTitle": "(Sin título)",
          "delete": "Eliminar",
          "deleteEvent": "Eliminar Horario",
          "deleteMultipleEvent": "Eliminar Múltiples Horarios",
          "selectedItems": "Elementos seleccionados",
          "deleteSeries": "Eliminar series",
          "edit": "Editar",
          "editSeries": "Editar",
          "editEvent": "Editar Horario",
          "createEvent": "Crear",
          "subject": "Asunto",
          "addTitle": "Agregar título",
          "moreDetails": "Más Detalles",
          "save": "Guardar",
          "editContent": "¿Desea editar el horario?",
          "deleteRecurrenceContent": "¿Desea eliminar el horario?",
          "deleteContent": "¿Está seguro de eliminar este horario?",
          "deleteMultipleContent": "¿Está seguro de eliminar estos horarios?",
          "newEvent": "Ingreso de horario",
          "title": "Título",
          "location": "Ubicación",
          "description": "Descripción",
          "timezone": "Zona horaria",
          "startTimezone": "Start Timezone",
          "endTimezone": "End Timezone",
          "repeat": "Se repite",
          "saveButton": "Guardar",
          "cancelButton": "Cancelar",
          "deleteButton": "Eliminar",
          "recurrence": "Recurrencia",
          "wrongPattern": "The recurrence pattern is not valid.",
          "seriesChangeAlert": "¿Los cambios se aplicarán a todas las repeticiones de este horario, está de acuerdo?",
          "createError": "The duration of the event must be shorter than how frequently it occurs. Shorten the duration, or change the recurrence pattern in the recurrence event editor.",
          "recurrenceDateValidation": "Some months have fewer than the selected date. For these months, the occurrence will fall on the last date of the month.",
          "sameDayAlert": "Two occurrences of the same event cannot occur on the same day.",
          "editRecurrence": "Editar recurrencia",
          "repeats": "Repeticiones",
          "alert": "Alerta",
          "alertContent": "Alerta",
          "startEndError": "The selected end date occurs before the start date.",
          "invalidDateError": "The entered date value is invalid.",
          "ok": "Ok",
          "occurrence": "Occurrence",
          "series": "Series",
          "previous": "Anterior",
          "next": "Siguiente",
          "timelineDay": "Timeline Day",
          "timelineWeek": "Timeline Week",
          "timelineWorkWeek": "Timeline Work Week",
          "timelineMonth": "Timeline Month"
        },
        "recurrenceeditor": {
          "none": "Niguno",
          "daily": "Diario",
          "weekly": "Semanalmente",
          "monthly": "Mensualmente",
          "month": "Mes",
          "yearly": "Anualmente",
          "never": "Nunca",
          "until": "Hasta",
          "count": "Número de veces",
          "first": "Primero",
          "second": "Segundo",
          "third": "Tercero",
          "fourth": "Cuarto",
          "last": "Último",
          "repeat": "Repetir",
          "repeatEvery": "Repetir cada",
          "on": "Repetir los días:",
          "end": "Fin",
          "onDay": "Día",
          "days": "Día(s)",
          "weeks": "Semana(s)",
          "months": "Mes(es)",
          "years": "Año(s)",
          "every": "cada",
          "summaryTimes": "vece(s)",
          "summaryOn": "on",
          "summaryUntil": "hasta",
          "summaryRepeat": "Repeticiones",
          "summaryDay": "día(s)",
          "summaryWeek": "semana(s)",
          "summaryMonth": "mes(es)",
          "summaryYear": "año(s)"
    
        },
        "calendar": {
          "today": "Hoy"
        },
        'formValidator': {
          "required": "Este campo es requerido",
          "max": "الرجاء إدخال قيمة أقل من أو تساوي {0}",
          "min": "الرجاء إدخال قيمة أكبر من أو تساوي {0}",
          "regex": "يرجى إدخال قيمة صحيحة",
          "tel": "يرجى إدخال رقم هاتف صالح",
          "pattern": "الرجاء إدخال قيمة نمط صحيح",
          "equalTo": "يرجى إدخال نص مطابقة صحيح"
        }
      }
    });
    
    this.cargarDatos()

  }
  cargarDatos() {
    this.obtenerCiclos();
   
    this.obtenerMaterias();

    this.eventSettings.dataSource = this.source

  }

  obtenerHorario() {

    this._angularPerformance.performance.subscribe(
      performance => {
        const trace = performance.trace('Obtener Horario');
        trace.start();

        this._horarioService.obtener().subscribe(
          horarios => {

            this.obtenerPersonal();
            this.listaTodosHorario = horarios.filter(item => { return item.tipo == 'H' })
            this.listaHorariosMapeada = this.listaTodosHorario.filter(
              horario => {
                return horario.fkLaboratorio.id == this.laboratorioSeleccionado
              }
            ).map(
              horario => {

                return this.mapearHorario(horario)
              }
            )

            this.listaHorariosMapeadaFiltrada = this.listaHorariosMapeada
            this.scheduleObj.eventSettings.dataSource = this.listaHorariosMapeadaFiltrada
            this.listaHorariosMapeadaSinFiltro = this.listaHorariosMapeadaFiltrada
            trace.putAttribute('RecuperacionHorario', 'Registros recuperados:' + this.listaTodosHorario.length);
            trace.stop();
          },
          error => {

            trace.putAttribute('ErrorRecuperacionHorarioo', error.error);
            trace.stop();
          }
        )
      }
    );
  }
  mapearHorario(horario: Reserva) {
    return {

      Id: horario.id,
      StartTime: horario.fechaInicio,
      EndTime: horario.fechaFin,
      idUsuario: horario.fkPersonaCrea.id,
      laboratorio: horario.fkLaboratorio.id,
      materia: horario.fkMateria.id,
      profesor: horario.fkPersonaReserva.id,
      RecurrenceRule: horario.recurrenceRule,
      Subject: horario.subject,


    }
  }
  profesor(idPersona) {
    var nombreProfesor = this.listaProfesores.find(x => { return x.id == idPersona })
    if(nombreProfesor==undefined){
      return ""
    }else{
      return nombreProfesor.nombreCompleto
    }
     


    
  }
  obtenerCiclos() {

    this._cicloService.obtener().subscribe(
      ciclos => {
        this.obtenerLaboratorios();
        this.listaCiclos = ciclos.sort((a, b) => {

          return (new Date(b.fechaInicio.toString()).getTime() - new Date(a.fechaInicio.toString()).getTime());

        })
        this.cicloTempral = this.auxListaCiclos[0]
        this.auxListaCiclos = this.listaCiclos.map(x => {
          return {
            label: x.nombre,
            value: x.id
          }
        })

        this.cicloSeleccionado = this.auxListaCiclos[0].value


        this.setearFechaCiclo(this.listaCiclos[0].id)


        //   this.cicloData = auxListaCiclos


      }
    )
  }
  onChangeCiclo(event: any) {
    this.setearFechaCiclo(event.value)
  }
  onChangeLaboratorio(event: any) {
    this.laboratorioTemporal = this.listaLaboratorios.find(lab => lab.id == event.value)
    this.scheduleObj.eventSettings.dataSource = this.listaTodosHorario.filter(
      horario => {
        return horario.fkLaboratorio.id == event.value
      }
    ).map(
      horario => {

        return this.mapearHorario(horario)
      }
    )
  }
  setearFechaCiclo(idCiclo) {
    this.cicloTempral = this.listaCiclos.find(ciclo => ciclo.id == idCiclo)
    var fechaTemporalFin = this.cicloTempral.fechaFin
    var fechaTemporalInicio = this.cicloTempral.fechaInicio
    this.fin_recurrenceEditor = "UNTIL="
    var fechaFin = new Date(fechaTemporalFin.toString())
    this.selectedDate = new Date(fechaTemporalInicio.toString())


    var dia = fechaFin.getDate() <= 10 ? '0' + fechaFin.getDate() : fechaFin.getDate()
    var mes = fechaFin.getMonth() <= 10 ? '0' + (fechaFin.getMonth() + 1) : fechaFin.getMonth()

    this.fin_recurrenceEditor = this.fin_recurrenceEditor.concat(fechaFin.getFullYear().toString()).concat(mes.toString()).concat(dia.toString())
    var x = document.getElementById("finCiclo")

  }
  obtenerMaterias() {
    this._materiaService.obtener().subscribe(
      materias => {
        this.listaMateria = materias
        var auxListaMaterias = this.listaMateria.map(x => {
          return {
            nombre: x.nombre,
            id: x.id,
            carrera: x.fkCarrera.nombre
          }
        })
        this.materiaData = auxListaMaterias

      }
    )

  }
  obtenerLaboratorios() {
    this._laboratorioService.obtener().subscribe(
      laboratorios => {
        this.listaLaboratorios = laboratorios.filter(item => { return item.estado == 'ACTIVO' })
        this.laboratorioTemporal = this.listaLaboratorios[0]
        this.auxListaLaboratorio = this.listaLaboratorios.map(x => {
          return {
            label: x.nombre,
            value: x.id
          }
        })
        this.laboratorioSeleccionado = this.auxListaLaboratorio[0].value
        this.obtenerHorario()
        // this.laboratorioData = auxListaLaboratorios
      }
    )
  }
  obtenerPersonal() {
    this._personaService.obtener().subscribe(
      persona => {
        this.listaProfesores = persona
        var auxListaProfesores = this.listaProfesores.map(x => {
          return {
            nombre: x.nombreCompleto,
            id: x.id
          }
        })
        this.personaData = auxListaProfesores
        //this.spinnerOverlayService.hide()
      }
    )
  }

  public oneventRendered(args: EventRenderedArgs): void {

    let categoryColor: string = args.data.CategoryColor as string;
    if (!args.element || !categoryColor) {
      return;
    }
    if (this.scheduleObj.currentView === 'Agenda') {
      (args.element.firstChild as HTMLElement).style.borderLeftColor = categoryColor;
    } else {
      args.element.style.backgroundColor = categoryColor;
    }
  }


  private instance: Internationalization = new Internationalization();
  getTimeString(value: Date): string {
    return this.instance.formatDate(value, { skeleton: 'hm' });
  }
  permiteEditar(idPersonaCrea: any): boolean {
    if (idPersonaCrea) {
      if ((this._sessionManagerService.checkTipoUsuario('Profesor') && idPersonaCrea != this._sessionManagerService.obteneridPersona())) {
        return true
      } else {
        return false
      }
    } else {
      return false
    }

  }

  onPopupOpen(args: PopupOpenEventArgs): void {
    this.alertaHoraInicioMenor= false;
    this.alertaHoraInicioMayor= false;
    var argsTemp: any = args.data
    if (args.type == "QuickInfo") {
      var dialogObj = args.element['ej2_instances'][0];
      dialogObj.hide();
      var currentAction: CurrentAction = args.target.classList.contains("e-work-cells") ? "Add" : "EditSeries"  ;
      this.scheduleObj.openEditor(args.data, currentAction);

    }
    this.seleccionarReservaAEditar(args.data)


    if (args.type === 'Editor') {

      let statusElement: HTMLInputElement = args.element.querySelector('#materia') as HTMLInputElement;
      let subjectElement: HTMLInputElement = args.element.querySelector('#Subject') as HTMLInputElement;
      let laboratorio: HTMLInputElement = args.element.querySelector('#laboratorio') as HTMLInputElement;
      let usuarioLogueado: HTMLInputElement = args.element.querySelector('#idUsuario') as HTMLInputElement;
      let cicloSeleccionado: HTMLInputElement = args.element.querySelector('#idCiclo') as HTMLInputElement;
      let statusElementProfesor: HTMLInputElement = args.element.querySelector('#profesor') as HTMLInputElement;

      if (!statusElement.classList.contains('e-dropdownlist')) {
        let dropDownListObject: DropDownList = new DropDownList({
          dataSource: this.materiaData,
          // maps the appropriate column to fields property
          fields: { groupBy: 'carrera', text: 'nombre', value: 'id' },

          //set the placeholder to DropDownList input
          placeholder: "Seleccione una materia",
          allowFiltering: true,
          change:function(args){
            if(args.itemData){
              subjectElement.value="GR1 - "+args.itemData['nombre'].toString()
            }
          }



        });
        dropDownListObject.setProperties({ validation: true })



        let dropDownListObjectPersona: DropDownList = new DropDownList({
          dataSource: this.personaData,
          // maps the appropriate column to fields property
          fields: { text: 'nombre', value: 'id' },
          //set the placeholder to DropDownList input
          placeholder: "Seleccione un profesor",
          allowFiltering: true,



        });

        if (this.laboratorioSeleccionado != null) {
          laboratorio.value = this.laboratorioSeleccionado
        }

        if (this.scheduleObj.currentAction == 'Add') {
          usuarioLogueado.value = this._sessionManagerService.obteneridPersona()
          statusElementProfesor.value = this._sessionManagerService.obteneridPersona()
        }


        dropDownListObject.appendTo(statusElement);
        if (statusElement.value != '')
          dropDownListObject.text = this.obtenerNombreMateria(statusElement.value)

        if (statusElementProfesor.value != '')
          dropDownListObjectPersona.text = this.obtenerNombreProfesor(statusElementProfesor.value)

        dropDownListObjectPersona.appendTo(statusElementProfesor);

      }

      var finCiclo = this.fin_recurrenceEditor

      if (!args.element.querySelector('.custom-field-row')) {
        var _this=this
        let startElement: HTMLInputElement = args.element.querySelector('#StartTime') as HTMLInputElement;
        var minStart=new Date(startElement.value) 
        minStart.setHours(7);
        var maxStart=new Date(startElement.value) 
        maxStart.setHours(21);
        if (!startElement.classList.contains('e-datetimepicker')) {
          var startTimePicker=new DateTimePicker({
            value: new Date(startElement.value) || new Date(),
            enabled: true,
            showClearButton:false,
            format: 'HH:mm',
            step: 60,
            min:minStart,
            max:maxStart,
            allowEdit:false,
            change: function(args){
              var minAux=new Date(args.value)
              minAux.setHours(minAux.getHours()+1)
              endTimePicker.min=minAux
              if(args.element['value'] >= endElement.value){
                endTimePicker.value=minAux

                endElement.value=minAux.toString().substr(16,5)
              }
              
            }
          }, startElement);
        }
        let endElement: HTMLInputElement = args.element.querySelector('#EndTime') as HTMLInputElement;
        var minEnd=new Date(endElement.value) 
        minEnd.setHours(8);
        var maxEnd=new Date(endElement.value) 
        maxEnd.setHours(22);
        if (!endElement.classList.contains('e-datetimepicker')) {
          var endTimePicker=new DateTimePicker({
            value: new Date(endElement.value) || new Date(),
            format: 'HH:mm',
            step: 60,
            showClearButton:false,
            allowEdit:false,
            min:minEnd,
            max:maxEnd,
          },
            endElement);
        }



        this.recurElement = args.element.querySelector('#RecurrenceEditorHorario');
        if (!this.recurElement.classList.contains('e-recurrenceeditor')) {
          var recurrenceRuleTemp = argsTemp.RecurrenceRule
          if (this.scheduleObj.currentAction == 'Add') {
            let byDayRecurrence = this.devuelveDiaParaRecurrencia(args.data['StartTime'])
            let fechaFinCiclo: Date = new Date(this.cicloTempral.fechaFin as Date)
            let untilDate = fechaFinCiclo.toISOString().replace(/-/g, '').substr(0, 8)
            recurrenceRuleTemp = 'FREQ=WEEKLY;INTERVAL=1;UNTIL=' + untilDate + 'T235823Z;BYDAY=' + byDayRecurrence;
          }
          this.recurrObject = new RecurrenceEditor({
            value: recurrenceRuleTemp,
            frequencies: ['weekly']


          });

          this.recurrObject.appendTo(this.recurElement);
          (this.scheduleObj.eventWindow as any).recurrenceEditor = this.recurrObject;
        }

        document.getElementById('RecurrenceEditorHorario').style.display = (this.scheduleObj.currentAction == "EditOccurrence") ? 'none' : 'block';
        cicloSeleccionado.value = this.cicloSeleccionado.toString()
        setCulture('es')


      }
    }
  }
  abrirModalFiltroLaboratorio() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.id = "filtro-laboratorio"
    dialogConfig.height = "350px";
    dialogConfig.width = "600px";
    dialogConfig.data = {
      data: "jdjd"
    }

    const modalDialog = this.matDialog.open(FiltroLaboratorioComponent, {
      data: {
        listaLaboratorios: this.listaLaboratorios
      }
    })
    modalDialog.afterClosed().subscribe(
      data => {
        var laboratorioSeleccionado = data
        this.laboratorioSeleccionado = data.idLaboratorio
        var lab = {
          value: data.idLaboratorio,
          label: data.nombreLaboratorio
        }
        this.onChangeLaboratorio(lab)
        this.mostrarMensaje("Laboratorio " + data.nombreLaboratorio + " seleccionado")
      }
    )

  }

  devuelveDiaParaRecurrencia(fechaTemporal: Date) {
    switch (fechaTemporal.getDay()) {
      case 1:
        return 'MO';
      case 2:
        return 'TU';
      case 3:
        return 'WE';
      case 4:
        return 'TH';
      case 5:
        return 'FR';
      case 6:
        return 'SA';
      case 7:
        return 'SU';

    }
  }

  // BYDAY=MO
  obtenerNombreMateria(id: string) {

    var idTemp: number = Number(id)
    var materiaTemporal = this.listaMateria.find(x => x.id == idTemp)
    return materiaTemporal.nombre
  }
  obtenerNombreCiclo(id: string) {

    var idTemp: number = Number(id)
    var cicloTemporal = this.listaCiclos.find(x => x.id == idTemp)
    return cicloTemporal.nombre
  }
  obtenerNombreProfesor(id: string) {
    if (this.listaProfesores.length != 0) {
      var idTemp: number = Number(id)
      var profesorTemporal = this.listaProfesores.find(x => x.id == idTemp)
      return profesorTemporal.nombreCompleto
    }
  }


  guardarLogReservaCrear(_reserva: Reserva) {
    var logReserva: LogReserva = {
      accion: "CREAR",
      actual: JSON.stringify(_reserva),
      anterior: "",
      idReserva: _reserva.id,
      idUsuario: Number(this._sessionManagerService.obteneridPersona())
    }
    this.scheduleObj.eventSettings.dataSource
    this._reservaService.guardarLogReserva(logReserva).subscribe(
      data => {

      }
    )

  }
  guardarLogReservaEliminar(_reserva: Reserva) {
    var logReserva: LogReserva = {
      accion: "ELIMINAR",
      actual: "",
      anterior: JSON.stringify(_reserva),
      idReserva: _reserva.id,
      idUsuario: Number(this._sessionManagerService.obteneridPersona())
    }
    this._reservaService.guardarLogReserva(logReserva).subscribe(
      data => {

      }
    )

  }
  guardarReserva(args) {

    var horarioTemporal: Reserva = {
      fechaInicio: args.StartTime,
      fechaFin: args.EndTime,
      fkCiclo: this.cicloSeleccionado,
      fkLaboratorio: this.laboratorioSeleccionado,
      fkPersonaCrea: this._sessionManagerService.obteneridPersona(),
      fkPersonaReserva: args.profesor,
      fkMateria: args.materia,
      subject: args.Subject,
      recurrenceRule: args.RecurrenceRule == null ? "" : args.RecurrenceRule,
      tipo: 'H'
    }
    this._angularPerformance.performance.subscribe(
      performance => {
        const trace = performance.trace('Crear Horario');
        trace.start();
        this._horarioService.crear(horarioTemporal).subscribe(
          horarioCreado => {
            this.guardarLogReservaCrear(horarioCreado)
            this.guardarReservaLocal(horarioCreado)
            this.onChangeLaboratorio({ value: this.laboratorioSeleccionado })
            this.mostrarMensaje('Horario registrado correctamente')
            trace.putAttribute('CreacionHorario', 'Horario creado:');
            trace.stop();
          },
          error => {
            trace.putAttribute('ErrorRegistroHorario', error.error);
            trace.stop();
            this.mostrarMensaje("Ha surgido un problema con la creación de horario");
          }
        )
      }
    )
  }
  guardarReservaLocal(horario: Reserva) {
    this.listaTodosHorario.push(horario)
    //this.listaHorariosMapeadaSinFiltro.push(this.mapearHorario(horario))
  }
  editarReservaLocal(horario: Reserva) {
    var indexHorario = this.listaTodosHorario.findIndex(item => item.id == horario.id)
    this.listaTodosHorario[indexHorario] = horario
  }


  editarReserva(args) {

    var horarioTemporal: Reserva = {
      id: this.idReservaSeleccionada,
      fechaInicio: args.StartTime,
      fechaFin: args.EndTime,
      fkCiclo: this.cicloSeleccionado,
      fkLaboratorio: this.laboratorioSeleccionado,
      fkPersonaCrea: this._sessionManagerService.obteneridPersona(),
      fkPersonaReserva: args.profesor,
      fkMateria: args.materia,
      subject: args.Subject,
      recurrenceRule: args.RecurrenceRule == null ? "" : args.RecurrenceRule,
      tipo: 'H'
    }
    this._reservaService.editarReserva(horarioTemporal, Number(this._sessionManagerService.obteneridPersona())).subscribe(
      horarioEditado => {
        this.editarReservaLocal(horarioEditado)
        this.mostrarMensaje('Horario editado correctamente')
      }
    )
  }
  eliminarReserva(args) {
    var horarioTemporal: Reserva = {
      id: this.idReservaSeleccionada,
      fechaInicio: args.StartTime,
      fechaFin: args.EndTime,
      fkCiclo: this.cicloSeleccionado,
      fkLaboratorio: this.laboratorioSeleccionado,
      fkPersonaCrea: this._sessionManagerService.obteneridPersona(),
      fkPersonaReserva: args.profesor,
      fkMateria: args.materia,
      subject: args.Subject,
      recurrenceRule: args.RecurrenceRule,
      tipo: 'H'
    }
    this._horarioService.eliminar(horarioTemporal).subscribe(
      horario => {

        this.guardarLogReservaEliminar(horario)
        this.eliminarResevaLocal(horario)
        this.mostrarMensaje('Horario eliminado correctamente')
      }
    )
  }
  eliminarResevaLocal(horario: Reserva) {
    this.scheduleObj.deleteEvent(horario.id)
    var indexHorario = this.listaTodosHorario.findIndex(item => item.id == horario.id)
    this.listaTodosHorario.splice(indexHorario, 1)
  }
  idReservaSeleccionada: any
  seleccionarReservaAEditar(args) {
    if (args.Id != undefined)
      this.idReservaSeleccionada = args.Id
  }
  mostrarMensaje(mensaje: string) {

    this._snackBar.openFromComponent(SnackbarVistaComponent, {
      data: { message: mensaje },
      duration: 3000
    });

  }
  ngAfterViewInit() {
    this.scheduleObj
  }
  add(): void {
    let Data: Object[] = [{
      Id: 1,
      Subject: 'Conference',
      StartTime: new Date(2018, 1, 15, 9, 0),
      EndTime: new Date(2018, 1, 15, 10, 0),
      IsAllDay: false,
      RecurrenceRule: 'FREQ=DAILY;INTERVAL=3;COUNT=4',
      CategoryColor: "#1AAA55"

    }];
    this.scheduleObj.addEvent(Data);
    this.scheduleObj.dataBind
    eventRendered: (args: EventRenderedArgs) => applyCategoryColor(args, this.scheduleObj.currentView)



  }

  actionComplete(args: ActionEventArgs) {
    switch (args.requestType) {
      case 'eventCreated':
        if (this.scheduleObj.isSlotAvailable(args.data[0])) {
          this.guardarReserva(args.addedRecords[0]);
        } else {
          this.mostrarMensaje("El horario se encuentra en conflicto con otro horario");
          args.cancel = true;
        }
        break;
      case 'eventChanged':
        this.editarReserva(args.changedRecords[0]);
        break;
      case 'eventRemoved':
        this.eliminarReserva(args.deletedRecords[0]);
        break;
    }
  }


}
