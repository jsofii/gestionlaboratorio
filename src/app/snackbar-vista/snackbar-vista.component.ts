import { Component, OnInit, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material';


@Component({
  selector: 'app-snackbar-vista',
  templateUrl: './snackbar-vista.component.html',
  styleUrls: ['./snackbar-vista.component.css']
})
export class SnackbarVistaComponent implements OnInit {

  constructor(
    @Inject(MAT_SNACK_BAR_DATA) public data: any
  ) { }

  ngOnInit() {
  }

  get getIcon() {
    switch (this.data.snackType) {
      case 'Success':
        return 'check_circle';
      case 'Error':
        return 'dnd_forwardslash';
      case 'Warning':
        return 'warning';
      case 'Info':
        return 'info';
      default:
        return 'info';
    }
  }

}
