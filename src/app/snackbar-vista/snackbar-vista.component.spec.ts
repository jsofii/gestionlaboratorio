import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SnackbarVistaComponent } from './snackbar-vista.component';

describe('SnackbarVistaComponent', () => {
  let component: SnackbarVistaComponent;
  let fixture: ComponentFixture<SnackbarVistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SnackbarVistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnackbarVistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
