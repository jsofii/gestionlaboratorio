import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PersonaService } from '../persona/persona.service';
import { Usuario, TipoPersona, Persona } from '../modelos/persona.data'
import { UsuarioService } from './usuario.service';
import { MatSnackBar } from '@angular/material';
import { v4 as uuid } from 'uuid';
import { SnackbarVistaComponent } from '../snackbar-vista/snackbar-vista.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirePerformance } from '@angular/fire/performance';
import { CorreoHttpService } from '../servicios/correo-http.service';
import { Encriptacion } from '../controladores/Encriptacion';
import { SessionManagerService } from '../servicios/session-manager.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {


  esAdministrador=false;
  esPasante=false;
  esProfesor=false;

  @ViewChild('cerrarModal') cerrarModal: ElementRef

  private encriptacion:Encriptacion=new Encriptacion();
  disableRegistrar:boolean=false;
  displayEliminar: boolean = false
  listaTiposPersona: TipoPersona[] = []
  usuarios: Usuario[] = []
  loading = true;

  columnas = [
    { field: 'fkPersona.nombreCompleto', header: 'Nombre Completo' },
    { field: 'fkPersona.correo', header: 'Correo' },
    { field: 'fkPersona.fkTipoPersona', header: 'Tipo' },
    { field: 'estado', header: 'Estado' },
    { field: 'activar', header: 'Activar / Desactivar' }
  ];

  public formCrearUsuario: FormGroup;

  constructor(private _correoHttp:CorreoHttpService,
    private _angularPerformance: AngularFirePerformance,
    private _sessionManager:SessionManagerService,
    private _angularFireAuth: AngularFireAuth,
    private _personaService: PersonaService,
    private _usuarioService: UsuarioService,
    private _snackBar: MatSnackBar,
    private fb: FormBuilder) { }


  ngOnInit() {
    this.obtenerTiposUsuariosBD();
    this.iniciarFormulario();
    this.checkUsuario();
  }

  checkUsuario(){
    
    if(this._sessionManager.checkTipoUsuario("Profesor")){
      this.esProfesor=true;
    }else if(this._sessionManager.checkTipoUsuario("Administrador")){
      this.esAdministrador=true;
    }else if(this._sessionManager.checkTipoUsuario("Pasante")){
      this.esPasante=true;
    }
  }

  iniciarFormulario() {
    this.formCrearUsuario = this.fb.group(
      {
        inputPersona: [{}, [Validators.required]]
      }
    )
  }

  mostrarMensaje(mensaje: string) {

    this._snackBar.openFromComponent(SnackbarVistaComponent, {
      data: { message: mensaje },
      duration: 5000
    });

  }

  limpiarFormCrearUsuario() {
    this.formCrearUsuario.patchValue({
      inputPersona: ''

    })
  }

  auxListaPersonas = [
    {
      label: 'PROFESORES',
      items: []
    },
    {
      label: 'PASANTES',
      items: []
    },
    {
      label: 'ADMINISTRADORES',
      items: []
    },
  ];

  abrirModalCrear() {
    this.auxListaPersonas = [
      {
        label: 'PROFESORES',
        items: []
      },
      {
        label: 'PASANTES',
        items: []
      },
      {
        label: 'ADMINISTRADORES',
        items: []
      },];

    this.limpiarFormCrearUsuario();
    this._personaService.obtener().subscribe(
      listaPersonas => {
        listaPersonas=listaPersonas.filter(item=>{return item.estado=='ACTIVO'})
        listaPersonas.forEach(persona => {
          
          if (persona.arregloUsuarios.length == 0) {
            if (persona.fkTipoPersona.nombre == "Administrador") {
              this.auxListaPersonas[2].items.push({ label: persona.nombreCompleto + ' | ' + persona.correo, value: persona })
            }
            if (persona.fkTipoPersona.nombre == "Pasante") {
              this.auxListaPersonas[1].items.push({ label: persona.nombreCompleto + ' | ' + persona.correo, value: persona })
            }
            if (persona.fkTipoPersona.nombre == "Profesor") {
              this.auxListaPersonas[0].items.push({ label: persona.nombreCompleto + ' | ' + persona.correo, value: persona })
            }
          }

        })
        this.auxListaPersonas[0].label = "PROFESORES: " + this.auxListaPersonas[0].items.length;
        this.auxListaPersonas[1].label = "PASANTES: " + this.auxListaPersonas[1].items.length;
        this.auxListaPersonas[2].label = "ADMINISTRADORES: " + this.auxListaPersonas[2].items.length;

      }, error => {
        this.mostrarMensaje("Error al cargar la lista de personas registradas");
      }
    )
  }

  obtenerTiposUsuariosBD() {
    this._personaService.obtenerlistaTiposPersonas().subscribe(
      tipos => {
        this.listaTiposPersona = tipos;
        this.obtenerUsuariosBD()
      }, error => {

        this.mostrarMensaje("Error al cargar los tipos de usuarios de la base de datos");
      }
    );
  }

  obtenerUsuariosBD() {
    this._angularPerformance.performance.subscribe(
      performance => {
        const trace = performance.trace('Obtener Usuarios');
        trace.start();
        this._usuarioService.obtener().subscribe(
          usuarios => {
            this.loading = false;
            this.usuarios = usuarios;
            usuarios.map(usuario => {
              usuario.fkPersona.fkTipoPersona = this.listaTiposPersona.find(tipo => tipo.id == usuario.fkPersona.fkTipoPersona).nombre;
            })

            trace.putAttribute('RecuperacionUsuarios', 'Registros recuperados:' + this.usuarios.length);
            trace.stop();

          }, error => {

            trace.putAttribute('ErrorRecuperacionUsuarios', error.error);
            trace.stop();

            this.mostrarMensaje("Error al cargar los usuarios de la base de datos");
          });


      }
    );

  }


  desactivarUsuario(usuario: Usuario) {
    if (usuario.estado == "INACTIVO") {
      usuario.estado = "ACTIVO"
    } else {
      usuario.estado = "INACTIVO"
    }

    let usuarioTemp: Usuario = {
      id: usuario.id,
      estado: usuario.estado
    }

    this._angularPerformance.performance.subscribe(
      performance => {
        const trace = performance.trace('Modificar Usuario');
        trace.start();
        this._usuarioService.editar(usuarioTemp).subscribe(
          usuarioEditado => {

            trace.putAttribute('ModificarUsuario', 'Tiempo empleado');
            trace.stop();

            this.mostrarMensaje("Cambios aplicados exitosamente");
            usuarioEditado.fkPersona.fkTipoPersona = this.listaTiposPersona.find(tipo => tipo.id == usuarioEditado.fkPersona.fkTipoPersona).nombre
            let index = this.usuarios.findIndex(usuario => usuario.id == usuarioEditado.id);
            this.usuarios[index] = usuarioEditado;
          }, error => {
            
            trace.putAttribute('ErrorModificarUsuario', error.error);
            trace.stop();

            this.mostrarMensaje("Error al editar los datos del usuario");
          }
        );
      });
  }



  registrarUsuario() {

    this._angularPerformance.performance.subscribe(
      performance => {
        this.disableRegistrar=true;
        const trace = performance.trace('Registrar Usuario');
        trace.start();
        let persona: Persona = this.formCrearUsuario.controls['inputPersona'].value;
        let passwordTemp: string = uuid().substring(0,7);
        let passwordEncrypted=this.encriptacion.encriptar(passwordTemp);
        var usuario: Usuario = {
          fkPersona: persona.id,
          password:passwordEncrypted.toString()
        }
        this._usuarioService.crear(usuario).subscribe(
          usuarioCreado => {

            trace.putAttribute('RegistrarUsuario', 'Tiempo empleado');
            trace.stop();
            this.mostrarMensaje("Registro realizado exitosamente");
            usuarioCreado.fkPersona.fkTipoPersona = this.listaTiposPersona.find(tipo => tipo.id == usuarioCreado.fkPersona.fkTipoPersona).nombre
            this.usuarios.push(usuarioCreado);
            this.cerrarModal.nativeElement.click()
            this.disableRegistrar=false;

            this._correoHttp.enviarCredencialesPorCorreo(persona.correo,passwordTemp).subscribe(
              exito=>{
                this.mostrarMensaje("Se enviará un correo electrónico a '"+persona.correo+"' con las credenciales de acceso");
              },
              error=>{
                this.mostrarMensaje("Ocurrió un error en el envío de las credenciales al correo '"+persona.correo+"'");
                console.log(error);
              }
            )


          }, error => {

            trace.putAttribute('ErrorRegistrarUsuario', error.error);
            trace.stop();
            this.disableRegistrar=false;

            this.mostrarMensaje("Ha surgido un problema con la creación del usuario");
          }
        );

      },error=>{
        console.log(error);
      }
      );

  }


}
