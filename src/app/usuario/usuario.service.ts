import { Injectable } from '@angular/core';
import { Persona, Usuario } from '../modelos/persona.data';
import { ServicioPrincipalService } from '../servicios/servicio-principal.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import {config} from '../controladores/Config';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService extends ServicioPrincipalService<Usuario> {
  constructor( private _httpClient: HttpClient) { 
    super(_httpClient, 'Usuario');
  }

  
  updatePassword(token:string,password:string):Observable<any>{
    let body={
      token:token,
      password:password
    }

    let url=config.baseUrl+'/updatePassword'
    return this.httpClient.post<any>(url,body);
  }

  iniciarSesion(correo:string,password:string):Observable<any>{
    let url=config.baseUrl+'/iniciarSesion?correo='+correo+"&password="+password
    return this.httpClient.get<any>(url)
  }
  solicitarRecuperacion(correo:string):Observable<any>{

    let url=config.baseUrl+'/solicitarRecuperacion?correo='+correo
    return this.httpClient.get<any>(url)

  }

  

}
