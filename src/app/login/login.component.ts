import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { SnackbarVistaComponent } from '../snackbar-vista/snackbar-vista.component';
import { MatSnackBar, MatButton } from '@angular/material';
import { Router } from '@angular/router';
import { PersonaService } from '../persona/persona.service';
import { UsuarioService } from '../usuario/usuario.service';
import { Usuario } from '../modelos/persona.data';
import { SessionManagerService } from '../servicios/session-manager.service';
import { Encriptacion } from '../controladores/Encriptacion';
import { v4 as uuid } from 'uuid';
import { CorreoHttpService } from '../servicios/correo-http.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {


  @ViewChild('cerrarModalRecuperacion') cerrarModalRecuperacion: MatButton;
  public formLogin: FormGroup;
  public formRecuperacion: FormGroup;
  public formRegistro: FormGroup;
  disableLoginButton = false;
  disableRecoverButton = false;
  disableRegisterButton = false;
  showPassword = false;
  numeroPrueba=5.679017954

  emailRegEx = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';

  constructor(private _correoHttp:CorreoHttpService,
    private _sessionManager: SessionManagerService,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private _router: Router,
    private _personaService: PersonaService,
    private _usuarioService: UsuarioService) { }

  ngOnInit() {
    
    this.iniciarFormulario();
  }
 
  iniciarFormulario() {
    this.formLogin = this.formBuilder.group(
      {
        correo: ['', [Validators.required, Validators.pattern(this.emailRegEx)]],
        password: ['', Validators.required]
      }
    );

    this.formRegistro = this.formBuilder.group(
      {
        correo: ['', [Validators.required, Validators.pattern(this.emailRegEx)]]
      }
    );

    this.formRecuperacion = this.formBuilder.group(
      {
        correo: ['', [Validators.required, Validators.pattern(this.emailRegEx)]]
      }
    );
  }


  mostrarMensaje(mensaje: string, type: string = "Info") {

    this._snackBar.openFromComponent(SnackbarVistaComponent, {
      data: { message: mensaje, snackType: type },
      duration: 5000
    });

  }

  recuperarPassword() {
    this.disableRecoverButton = true;
    let correo = this.formRecuperacion.controls['correo'].value;
    this._usuarioService.solicitarRecuperacion(correo).subscribe(
      resultado => {
        if (resultado.estado) {
          this.cerrarModalRecuperacion._elementRef.nativeElement.click()
          this.mostrarMensaje('Se ha enviado un enlace para recuperar su contraseña al correo electrónico: ' + correo, "Success");
        } else {

          this.mostrarMensaje(resultado.mensaje, "Warning")
        }
        this.disableRecoverButton = false;
      }, error => {
        console.log(error)
        this.disableRecoverButton = false;
        this.mostrarMensaje('No se ha podido enviar un correo de recuperación al correo electrónico: ' + correo, "Error");

      }
    )
  }

  encriptacion: Encriptacion = new Encriptacion();
  iniciarSesion() {
    this.disableLoginButton = true;

    let correo = this.formLogin.controls['correo'].value;
    let password = this.formLogin.controls['password'].value;
    let passwordEncrypted = this.encriptacion.encriptar(password);
    this._usuarioService.iniciarSesion(correo, passwordEncrypted).subscribe(
      resultado => {
        this.disableLoginButton = false;
        this._sessionManager.guardarIdPersona(resultado.user.idPersona)
      
        if (resultado.user.estado == "ACTIVO") {
          if (resultado.login) {

            this.mostrarMensaje('Inicio de sesion exitoso', "Success");
            this._sessionManager.setSession(resultado.user.tipoUsuario, resultado.user.idUsuario);
            this._router.navigate(['/app/principal']);
          } else {
            this.mostrarMensaje("El usuario tiene el acceso denegado para acceder al sistema", "Error")
          }
        } else {
          this.mostrarMensaje('Las Credenciales son Incorrectas', "Error");
        }
      },
      error => {
        this.disableLoginButton = false;
        console.log(error)
        this.mostrarMensaje("Error con el inicio de sesión", "Error")
      }
    );

  }


  registrarUsuario() {
    let correo: string = this.formRegistro.controls['correo'].value;

    this.disableRegisterButton = true;
    this._personaService.obtenerPorCorreo(correo).subscribe(
      persona => {
        if (persona.length > 0) {
          if (persona[0].arregloUsuarios.length == 0) {
            
            let passwordTemp: string = uuid().substring(0,7);
            let passwordEncrypted = this.encriptacion.encriptar(passwordTemp);
            var usuario: Usuario = {
              fkPersona: persona[0].id,
              password: passwordEncrypted
            }
            this._usuarioService.crear(usuario).subscribe(
              usuarioCreado => {
                this.disableRegisterButton = false;
                this.mostrarMensaje("Registro realizado exitosamente");
                this._sessionManager.setSession(persona[0].fkTipoPersona.nombre, usuarioCreado.id);

                this._correoHttp.enviarCredencialesPorCorreo(correo,passwordTemp).subscribe(
                  exito=>{
                    this.mostrarMensaje("Se ha enviado un correo electrónico a '"+correo+"' con las credenciales de acceso");
                    this._router.navigateByUrl("/login#login");
                  },
                  error=>{
                    this.mostrarMensaje("Ocurrió un error en el envío de las credenciales al correo '"+correo+"'");
                    console.log(error);
                  }
                )

              }, error => {
                this.disableRegisterButton = false;
                console.log(error);
                this.mostrarMensaje("Ha surgido un problema con la creación del usuario", "Warning");
                //AGREGAR LOG DE ERROR SIGNUP
              }
            );

          } else {
            this.disableRegisterButton = false;
            this.mostrarMensaje("El correo ya se encuentra registrado en el sistema", "Error")
          }


        } else {

          this.disableRegisterButton = false;
          this.mostrarMensaje("El correo no se encuentra registrado/aprobado para registrarse en el sistema", "Error")
        }
      },
      error => {
        //AGREGAR LOG DE ERROR GET NOMINA
        this.disableRegisterButton = false;
        this.mostrarMensaje("Error al intentar acceder a la nómina de usuarios", "Warning")
      }
    );

  }
  mostrarContrasenia(){
   this.showPassword = this.showPassword == true ? false : true
  }

}
