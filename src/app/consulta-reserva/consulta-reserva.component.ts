import { Component, OnInit } from '@angular/core';
import { ReservaService } from '../reserva/reserva.service';
import {Reserva} from '../modelos/reserva.data'
import { SessionManagerService } from '../servicios/session-manager.service';
import { SnackbarVistaComponent } from '../snackbar-vista/snackbar-vista.component';
import { MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-consulta-reserva',
  templateUrl: './consulta-reserva.component.html',
  styleUrls: ['./consulta-reserva.component.css']
})
export class ConsultaReservaComponent implements OnInit {

  reservas:Reserva[]=[];

  columnas = [
    { field: 'fkPersonaReserva.nombreCompleto', header: 'Reservado Para' },
    { field: 'fkLaboratorio.nombre', header: 'Laboratorio' },
    { field: 'subject', header: 'Asunto' },
    {field:'fechaInicio',header:'Inicio'},
    {field:'fechaFin',header:'Fin'},

  ];

  constructor(private _reservaService:ReservaService, private _sessionManager:SessionManagerService, private _snackBar:MatSnackBar) { }

  ngOnInit() {
    if(this._sessionManager.checkTipoUsuario('Profesor')){
      this.obtenerReservasDelUsuario(this._sessionManager.obteneridPersona());
    }else{
      this.obtenerTodasLasReservas()
    }
  }

  mostrarMensaje(mensaje: string, type: string = "Info") {

    this._snackBar.openFromComponent(SnackbarVistaComponent, {
      data: { message: mensaje, snackType: type },
      duration: 5000
    });

  }


  obtenerTodasLasReservas(){
    this._reservaService.obtenerReservas().subscribe(
      reservas=>{
        this.reservas=reservas.map(reserva=>{
          reserva.fechaInicio=new Date(reserva.fechaInicio.toString());
          reserva.fechaFin=new Date(reserva.fechaFin.toString());
          return reserva;

        });
      },error=>{
        this.mostrarMensaje("Error al cargar las reservas","Error")
        console.log(error);
      }
    )
  }

  obtenerReservasDelUsuario(idUsuario){
    this._reservaService.obtenerReservasDelUsuario(idUsuario).subscribe(
      reservas=>{
        this.reservas=reservas.map(reserva=>{
          reserva.fechaInicio=new Date(reserva.fechaInicio.toString());
          reserva.fechaFin=new Date(reserva.fechaFin.toString());
          return reserva;

        });
      },error=>{
        this.mostrarMensaje("Error al cargar las reservas del usuario","Error")
        console.log(error);
      }
    )

  }

}
