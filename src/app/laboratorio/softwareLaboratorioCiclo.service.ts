import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from '@angular/fire/firestore'
import {Laboratorio, SoftwarePorLaboratorio} from '../modelos/laboratorio.data'
import { ServicioPrincipalService } from '../servicios/servicio-principal.service';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class SoftwareLaboratorioCicloService extends ServicioPrincipalService<SoftwarePorLaboratorio> {

  constructor(private _httpClient: HttpClient) {
    super(_httpClient, 'SoftwareLaboratorioCiclo');
   }

}
