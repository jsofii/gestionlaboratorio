import { Component, OnInit, ViewChild, ElementRef, forwardRef, Inject } from '@angular/core';
import { LaboratorioService } from './laboratorio.service';
import { Laboratorio, SoftwarePorLaboratorio } from '../modelos/laboratorio.data'
import { v4 as uuid } from 'uuid';
import { Software } from '../modelos/software.data';
import { SoftwareService } from '../software/software.service'
import { CicloService } from '../ciclo/ciclo.service';
import { Ciclo } from '../modelos/ciclo.data';
import { SoftwareLaboratorioCicloService } from './softwareLaboratorioCiclo.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { SnackbarVistaComponent } from '../snackbar-vista/snackbar-vista.component';
import { Dropdown } from 'primeng/dropdown';
import { Ordenador } from '../modelos/ordenador.data';
import { resolveReflectiveProviders } from '@angular/core/src/di/reflective_provider';
import { AngularFirePerformance } from '@angular/fire/performance';

@Component({
  selector: 'app-laboratorio',
  templateUrl: './laboratorio.component.html',
  styleUrls: ['./laboratorio.component.css']
})
export class LaboratorioComponent implements OnInit {

  @ViewChild('cerrarModal') cerrarModal: ElementRef
  @ViewChild('dropDownCiclo') dropDownCiclo: Dropdown
  listaLaboratorios: Laboratorio[] = []

  cicloSeleccionado: Ciclo = {} as Ciclo;

  laboratorioTemporal: Laboratorio = {
    nombre: "",
    numero: "",
    arregloSoftwareLaboratorioCiclo: []
  }

  tituloModalEditar: string;
  ordenadorTemporal: any;
  mostrarModalCrearP: boolean = false;
  esNuevoRegistro: boolean = false;
  softwareInstalado: Software[] = []
  softwareNoInstalado: any[] = []
  listaSoftwareInstalado: Software[] = []
  listaSoftwareNoInstalado: Software[] = []
  listaCiclos: Ciclo[] = [] as Ciclo[]
  listaSoftware: Software[] = [] as Software[]
  listaOrdenadores: Ordenador[] = [] as Ordenador[]
  auxListaCiclos: any[] = [] as any[]
  auxListaOrdenador: any[] = [] as any[]
  auxListasistemasOperativos: any[] = [] as any[]
  auxListaProcesadores: any[] = [] as any[]
  tempListaSoftwareLaboratorioCiclo: SoftwarePorLaboratorio[] = [] as SoftwarePorLaboratorio[]
  defListaSoftwareLaboratorioCiclo: SoftwarePorLaboratorio[] = [] as SoftwarePorLaboratorio[]
  tempsoftwareLaboratorioCiclo: SoftwarePorLaboratorio = {} as SoftwarePorLaboratorio

  tempLaboratorioSeleccionado: Laboratorio = {} as Laboratorio
  formCrearLaboratorio: FormGroup
  columnas = [

    { field: 'nombre', header: 'Nombre' },
    { field: 'numero', header: 'Numero' },
    { field: 'software', header: 'Software' },
    { field: 'acciones', header: 'Acciones' },
    { field: 'activar', header: 'Activar/Desactivar' }
  ];

  columnasSoftware = [

    { field: 'nombre', header: 'Nombre' },
    { field: 'numero', header: 'Numero' },

  ];

  columnasLabSoftware = [

    { field: 'nombre', header: 'Nombre' },
    { field: 'opciones', header: '' },

  ];

  constructor(@Inject(forwardRef(() => SoftwareLaboratorioCicloService)) private _softwareLaboratorioCicloService: SoftwareLaboratorioCicloService, private _laboratorioService: LaboratorioService, private _softwareService: SoftwareService,
    private _cicloService: CicloService,
    private fb: FormBuilder, private _snackBar: MatSnackBar,
    private _angularPerformance: AngularFirePerformance) {

  }

  ngOnInit() {
    this.cargarDatos();
    this.iniciarFormulario()
  }
  iniciarFormulario() {
    this.cicloSeleccionado.id = 0;
    this.formCrearLaboratorio = this.fb.group(
      {
        nombre: ['', [Validators.required]],
        numero: ['', [Validators.required]],
        ram: ['', [Validators.required]],
        sistemaOperativo: ['', [Validators.required]],
        procesador: ['', [Validators.required]],
        id: [''],
        idOrdenador: [''],
        capacidad: ['', [Validators.required]]

      }
    )
  }


  cargarDatos() {
    this.obtenerCiclos();
    this.obtenerLaboratorio();
    this.obtenerSoftware();
    this.obtenerProcesadores();
    this.obtenerSistemasOperativos();
  }
  obtenerCiclos() {
    this._cicloService.obtener().subscribe(
      ciclos => {
        this.listaCiclos = ciclos

        this.auxListaCiclos = this.listaCiclos.map(x => {
          return {
            label: x.nombre,
            value: x
          }
        })

      }
    )
  }

  obtenerLaboratorio() {
    this._angularPerformance.performance.subscribe(
      performance => {
        const trace = performance.trace('Obtener Laboratorios');
        trace.start();
        this.listaOrdenadores
        this._laboratorioService.obtener().subscribe(
          laboratorios => {
            this.listaLaboratorios = laboratorios
            this.obtenerOrdenadores();
            trace.putAttribute('RecuperacionLaboratorio', 'Registros recuperados:' + this.listaLaboratorios.length);
            trace.stop();
          },
          error => {

            trace.putAttribute('ErrorRecuperacionLaboratorio', error.error);
            trace.stop();
          }
        )
      }
    );
  }
  obtenerSistemasOperativos() {
    this._laboratorioService.obtenerSistemasOperativos().subscribe(
      sistemasOperativso => {
        this.auxListasistemasOperativos = sistemasOperativso.map(x => {
          return {
            label: x.nombre,
            value: x.id
          }
        })
      }
    )
  }
  obtenerProcesadores() {
    this._laboratorioService.obtenerProcesadores().subscribe(
      procesadores => {
        this.auxListaProcesadores = procesadores.map(x => {
          return {
            label: x.nombre,
            value: x.id
          }
        })
      }
    )
  }


  obtenerOrdenadores() {
    this._laboratorioService.obtenerOrdenadores().subscribe(
      ordenadores => {
        this.listaOrdenadores = ordenadores

        this.listaLaboratorios = this.listaLaboratorios.map(
          labo => {
            labo.arregloOrdenador[0] = this.listaOrdenadores.find(ord => {
              var labTemp: any = ord.fkLaboratorio
              return labTemp.id == labo.id
            })

            return labo
          }

        )
      }
    )
  }
  obtenerSoftware() {
    this._softwareService.obtener().subscribe(
      software => {
        this.listaSoftware = software.filter(item=>{return item.estado=='ACTIVO'})
      }
    )
  }
  limpiarRegistro() {
    this.formCrearLaboratorio.patchValue({
      nombre: "",
      numero: "",
      estado: "",
      ram: "",
      capacidad: "",
      sistemaOperativo: "",
      procesador: this.auxListaProcesadores[0].id



    })
  }
  limbiarSoftwarePorLaboratorioCiclo() {
    this.listaSoftwareNoInstalado = [] as Software[]
    this.listaSoftwareInstalado = [] as Software[]
  }
  guardarSistemasOperativosOrdenaodr(idOrdenador) {
    var listaSistemaOperativos = this.formCrearLaboratorio.value.sistemaOperativo
    this._laboratorioService.crearSistemaOpeativoOrdenador(listaSistemaOperativos, idOrdenador).subscribe(
      result => {


        alert(result)
      }
    )
  }
  guardarLaboratorio() {
    this.formCrearLaboratorio

    if (this.esNuevoRegistro) {
      this.crearLaboratorio();

    } else {
      this.editarLaboratorio();
    }


    this.cerrarModal.nativeElement.click()
  }

  editarLaboratorio() {
    var laboratorio: Laboratorio = {
      id: this.formCrearLaboratorio.controls['id'].value,
      nombre: this.formCrearLaboratorio.controls['nombre'].value,
      numero: this.formCrearLaboratorio.controls['numero'].value,
      capacidad: this.formCrearLaboratorio.controls['capacidad'].value

    }

    this._laboratorioService.editar(laboratorio).subscribe(
      laboratorioEditado => {

        var ordenadorTemp: Ordenador = {
          id: this.ordenadorTemporal.id,
          fkLaboratorio: laboratorioEditado.id,
          fkProcesador: this.formCrearLaboratorio.value.procesador,
          ram: this.formCrearLaboratorio.value.ram
        }
        this._laboratorioService.editarOrdenador(ordenadorTemp).subscribe(
          ordenadorTemp => {
            this._laboratorioService.crearSistemaOpeativoOrdenador(this.formCrearLaboratorio.value.sistemaOperativo, ordenadorTemp.id).subscribe(
              result => {
                this.cerrarModal.nativeElement.click()
                this.mostrarMensaje("Laboratorio registrado exitosamente!")
                this.limpiarFormCrearLaboratorio();
              }
            )
          }
        )

        let index = this.listaLaboratorios.findIndex(labo => labo.id == laboratorioEditado.id)
        this.listaLaboratorios[index] = laboratorioEditado;
        this.mostrarMensaje("Laboratorio editado exitosamente!")

      }
    );

  }
  editarOrdenadorLaboratorio(ordenador: Ordenador) {
    this._laboratorioService.guardarOrdenador(ordenador).subscribe(
      ordenador => {
        this.guardarSistemasOperativosOrdenaodr(ordenador.id)
      }
    )
  }
  crearLaboratorio() {
    var laboratorio: Laboratorio = {
      nombre: this.formCrearLaboratorio.controls['nombre'].value,
      numero: this.formCrearLaboratorio.controls['numero'].value,
      capacidad: this.formCrearLaboratorio.value.capacidad,
      estado: "ACTIVO"
    }
    this._angularPerformance.performance.subscribe(
      performance => {
        const trace = performance.trace('Crear Laboratorio');
        trace.start();
        this._laboratorioService.crear(laboratorio).subscribe(
          laboratorioCreado => {

            this.listaLaboratorios.push(laboratorioCreado);
            var ordenadorTemp: Ordenador = {
              fkLaboratorio: laboratorioCreado.id,
              fkProcesador: this.formCrearLaboratorio.value.procesador,
              ram: this.formCrearLaboratorio.value.ram
            }
            this._laboratorioService.guardarOrdenador(ordenadorTemp).subscribe(
              ordenadorTemp => {
                this._laboratorioService.crearSistemaOpeativoOrdenador(this.formCrearLaboratorio.value.sistemaOperativo, ordenadorTemp.id).subscribe(
                  result => {
                    this.cerrarModal.nativeElement.click()
                    this.mostrarMensaje("Laboratorio registrado exitosamente!")
                    this.limpiarFormCrearLaboratorio();
                  }
                )
              }
            )

            trace.putAttribute('CreacionLaboratorio', 'Laboratorio creado:');
            trace.stop();
          },
          error => {
            trace.putAttribute('ErrorRegistroLaborario', error.error);
            trace.stop();
            this.mostrarMensaje("Ha surgido un problema con la creación de laboratorio");
          }
        )
      }
    )
  }
  crearOrdenador() {

  }
  limpiarFormCrearLaboratorio() {
    this.formCrearLaboratorio.patchValue({
      nombre: "",
      numero: "",
      estado: "",
      ram: "",
      capacidad: "",
      sistemaOperativo: "",
      procesador: this.auxListaProcesadores[0].id



    })
  }

  mostrarMensaje(mensaje: string) {

    this._snackBar.openFromComponent(SnackbarVistaComponent, {
      data: { message: mensaje },
      duration: 3000
    });

  }
  seleccionarVerSoftwareLaboratorio(listaSoftwareLaboratorioCiclo: any[], laboratorio: Laboratorio) {

    this.laboratorioTemporal = laboratorio

    this.tempListaSoftwareLaboratorioCiclo = listaSoftwareLaboratorioCiclo
    this.defListaSoftwareLaboratorioCiclo = listaSoftwareLaboratorioCiclo
    if (this.cicloSeleccionado.id != 0) {
      this.filtrarSoftwareDeLaboratorio()
    }
    this.obtenerSoftwareDeLaboratorio()
  }
  obtenerSoftwareDeLaboratorio() {
    this.limbiarSoftwarePorLaboratorioCiclo()

    this.filtrarSoftwareDeLaboratorio()


  }
  filtrarSoftwareDeLaboratorio() {

    this.listaSoftwareNoInstalado = this.listaSoftware.filter(
      softwareEnLaboratorio => {

        return !this.verificarExistenciaSoftwareEnLaboratorio(softwareEnLaboratorio, this.tempListaSoftwareLaboratorioCiclo)
      }
    )

  }


  verificarExistenciaSoftwareEnLaboratorio(softwareAux: Software, listaLaboCicloSW: any[]): Boolean {
    var listaAux = listaLaboCicloSW.map(x => {
      return x.fkSoftware
    })
    var estaEnLaboratorio = listaAux.includes(softwareAux.id)
    if (estaEnLaboratorio) {
      this.listaSoftwareInstalado.push(softwareAux)
    }
    return estaEnLaboratorio;
  }


  obtenerListaSoftware(softwareLaboratorioSeleccionado: Laboratorio) {
    this.laboratorioTemporal = softwareLaboratorioSeleccionado
    var listaSWLabSeleccionado: String[]

  }
  desactivarLaboratorio(laboratorio: Laboratorio) {
    if (laboratorio.estado == "INACTIVO") {
      laboratorio.estado = "ACTIVO"
    } else {
      laboratorio.estado = "INACTIVO"
    }

    let laboTemp: Laboratorio = {
      id: laboratorio.id,
      estado: laboratorio.estado
    }

    this._laboratorioService.editar(laboTemp).subscribe(
      data => {
        this.mostrarMensaje("Cambios aplicados exitosamente")
        this.obtenerLaboratorio();
      }
    )
  }


  imprimir() {
    var x = this.softwareInstalado
    var y = this.softwareNoInstalado
  }

  abrirModalCrear() {
    this.limpiarRegistro()
    this.tituloModalEditar = "Registro de laboratorio"
    this.esNuevoRegistro = true;

  }

  abrirModalEditar(laboratorio: Laboratorio) {
    var ordenador: any = laboratorio.arregloOrdenador[0]
    this.listaLaboratorios
    this.tituloModalEditar = "Modificar laboratorio"
    this.esNuevoRegistro = false
    this._laboratorioService.obtenerOrdenadores().subscribe(
      ordenadores => {
        var ordenador: Ordenador = ordenadores.find(
          x => {
            this.ordenadorTemporal = x
            return this.ordenadorTemporal.fkLaboratorio.id == laboratorio.id
          }
        )
        var ordenadorTemp: any = ordenador
        this.formCrearLaboratorio.patchValue({
          nombre: laboratorio.nombre,
          numero: laboratorio.numero,
          id: laboratorio.id,
          capacidad: laboratorio.capacidad,
          ram: ordenadorTemp.ram,
          idOrdenador: ordenadorTemp.id,
          procesador: ordenadorTemp.fkProcesador.id,
          sistemaOperativo: ordenadorTemp.arregloOrdenadorSistemaOperativo.map(x => { return x.fkSistemaOperativo })

        })
      }
    )



  }

  displayEliminar = false;


  clearLaboratorio() {

  }


  agregarSoftwareALaboratorio(software: Software) {
    this._laboratorioService.crearSoftwareLaboratorioCiclo({

      fkLaboratorio: this.laboratorioTemporal.id,
      fkSoftware: software.id

    }).subscribe(
      softwareLaboratorioCiclo => {
        this.listaSoftwareInstalado.push(software)
        this.listaSoftwareNoInstalado.splice(this.listaSoftwareNoInstalado.indexOf(software), 1)
        this.obtenerLaboratorio();
      }
    )

  }

  eliminarSoftwareDeLaboratorio(software: Software) {
    this._laboratorioService.encontrarPoridSoftwareLaboratorioCiclo(
      {

        fkLaboratorio: this.laboratorioTemporal.id,
        fkSoftware: software.id
      }
    ).subscribe(
      softwareLAboratorioCicloAEliminar => {
        this._laboratorioService.elimiarSoftwareLaboratorioCiclo(softwareLAboratorioCicloAEliminar[0].id).subscribe(
          softwareLaboratorioCicloEliminado => {
            this.listaSoftwareNoInstalado.push(software)
            this.listaSoftwareInstalado.splice(this.listaSoftwareInstalado.indexOf(software), 1)
            this.obtenerLaboratorio();
          }
        )
      }
    )

  }

}
