import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from '@angular/fire/firestore'
import {Laboratorio, SoftwarePorLaboratorio} from '../modelos/laboratorio.data'
import { ServicioPrincipalService } from '../servicios/servicio-principal.service';
import { HttpClient } from '@angular/common/http';
import { config } from '../controladores/Config';
import { Ordenador } from '../modelos/ordenador.data';
import { SistemaOperativo, ordenadorSistemaOperativo } from '../modelos/sistemaOperativo.data';
import { Procesador } from '../modelos/procesador.data';


@Injectable({
  providedIn: 'root'
})
export class LaboratorioService extends ServicioPrincipalService<Laboratorio> {

  constructor(private _httpClient: HttpClient) {
    super(_httpClient, 'Laboratorio');
   }
   crearSoftwareLaboratorioCiclo(softwarePorLaboratorio: SoftwarePorLaboratorio){
    return this._httpClient.post<SoftwarePorLaboratorio>(config.baseUrl+'/SoftwareLaboratorio', softwarePorLaboratorio)
    
   }
   crearSistemaOpeativoOrdenador(varlistaSistemasOperativos, idOrdenador){
     var body={
      listaSistemasOperativos: varlistaSistemasOperativos
     }
    return this._httpClient.post<Boolean>(config.baseUrl+'/registrarSistemasOperativos?idOrdenador='+idOrdenador,varlistaSistemasOperativos)
   }
   guardarCaracterísticasOrdenador(sistemaOperativoOrdenador){
     
    return this._httpClient.post<SoftwarePorLaboratorio>(config.baseUrl+'/registrarSistemasOperativos', sistemaOperativoOrdenador)
    
   }
   guardarOrdenador(ordenador: Ordenador){
    return this.httpClient.post<Ordenador>(config.baseUrl+'/ordenador/', ordenador)
   }
   editarOrdenador(ordenador: Ordenador){
    return this.httpClient.patch<Ordenador>(config.baseUrl+'/ordenador/'+ordenador.id, ordenador)
   }
   obtenerOrdenadorSistemaOperativo(){
    return this.httpClient.get<ordenadorSistemaOperativo[]>(config.baseUrl+'/ordenadorSistemaOperativo')
   }
   
   obtenerOrdenadores(){
    return this._httpClient.get<Ordenador[]>(config.baseUrl+'/Ordenador')
   }
   obtenerProcesadores(){
    return this._httpClient.get<Procesador[]>(config.baseUrl+'/Procesador')
   }
   obtenerSistemasOperativos(){
    return this._httpClient.get<SistemaOperativo[]>(config.baseUrl+'/SistemaOperativo')
   }
   elimiarSoftwareLaboratorioCiclo(id:number){

    return this._httpClient.delete<SoftwarePorLaboratorio>(`${config.baseUrl}/SoftwareLaboratorio/${id}`)
   }
   encontrarPoridSoftwareLaboratorioCiclo(softwarePorLaboratorio: SoftwarePorLaboratorio){
    return this._httpClient.get<SoftwarePorLaboratorio>(`${config.baseUrl}/SoftwareLaboratorio?fkLaboratorio=${softwarePorLaboratorio.fkLaboratorio}&fkSoftware=${softwarePorLaboratorio.fkSoftware}`)
   }
   

}
