// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebaseConfig:{
    apiKey: "AIzaSyDU7ZIoEhp6p0WDP_9L4KX9XDHO8T5talk",
    authDomain: "sistema-de-gestion-labfis.firebaseapp.com",
    databaseURL: "https://sistema-de-gestion-labfis.firebaseio.com",
    projectId: "sistema-de-gestion-labfis",
    storageBucket: "sistema-de-gestion-labfis.appspot.com",
    messagingSenderId: "627161625662",
    appId: "1:627161625662:web:a2b94f84987a0c65e38d6f"
  },
 // baseUrl: "http://localhost:4009",
  baseUrl: "https://backendtesis.herokuapp.com",
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
